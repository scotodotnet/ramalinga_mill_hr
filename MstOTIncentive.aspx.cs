﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

public partial class MstOTIncentive : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionEpay;
    string SSQL = "";

    bool ErrFlg = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();

        if (!IsPostBack)
        {
            Load_OLD_Data();
        }
    }

    private void Load_OLD_Data()
    {
        SSQL = "";
        SSQL = "Select * from MstOTIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtOTAmt.Text = dt.Rows[0]["OTAmt"].ToString();
        }

        SSQL = "";
        SSQL = "Select * from MstWeekOFFIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
      //  DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtWeekAmt.Text = dt.Rows[0]["Amt"].ToString();
        }
        //Repeater1.DataSource = dt;
        //Repeater1.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        //if ((txtOTHrsFm.Text == "" || txtOTHrsTo.Text == "")|| (txtOTHrsFm.Text == "00.00" || txtOTHrsTo.Text == "00.00"))
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "SaveMsgAlert('Please Enter the OT hrs Properly');", true);
        //    ErrFlg = true;
        //}
        if (txtOTAmt.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "SaveMsgAlert('Please Enter the OT Incentive Amount');", true);
            ErrFlg = true;
        }
        if (!ErrFlg)
        {   //hide this for non repeater

            //SSQL = "";
            //SSQL = "Select * from MstOTIncentive where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and (OTHrsTo>=Convert(decimal(18,2),'" + txtOTHrsFm.Text + "') and OTHrsTo<=Convert(decimal(18,2),'" + txtOTHrsTo.Text + "')) or";
            //SSQL = SSQL + " (OTHrsFm>=Convert(decimal(18,2),'" + txtOTHrsFm.Text + "') and OTHrsFm<=Convert(decimal(18,2),'" + txtOTHrsTo.Text + "'))";
            //DataTable dt_check = new DataTable();
            //dt_check = objdata.RptEmployeeMultipleDetails(SSQL);
            //if (dt_check.Rows.Count > 0)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "SaveMsgAlert('OT Hrs time Already Present. Please Check!');", true);
            //    ErrFlg = true;
            //}
            //else
            //{
            //for non repeater
            SSQL = "";
            SSQL = "Delete from MstOTIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Delete from MstWeekOFFIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "insert into MstOTIncentive values('" + SessionCcode + "','" + SessionLcode + "','" + txtOTHrsFm.Text + "','" + txtOTHrsTo.Text + "','" + txtOTAmt.Text + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "insert into MstWeekOFFIncentive values('" + SessionCcode + "','" + SessionLcode + "','" + txtWeekAmt.Text + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);
            //}
            if (!ErrFlg)
            {
                if (btnSave.Text == "Save")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "SaveMsgAlert('Incentive Saved Successfully');", true);
                    btnCancel_Click(sender, e);
                }
                if (btnSave.Text == "Update")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "SaveMsgAlert('Incentive Updated Successfully');", true);
                    btnCancel_Click(sender, e);
                }
            }
            Load_OLD_Data();
        }
    }
    
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtOTHrsFm.Text = "00.00";
        txtOTHrsTo.Text = "00.00";
        txtOTAmt.Text = "0";
        txtWeekAmt.Text="0";
        btnSave.Text = "Save";
    }

    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string from = "";
        string To = "";

        from = e.CommandName.Split(',').FirstOrDefault();
        To = e.CommandName.Split(',').LastOrDefault();
        SSQL = "";
        SSQL = "Select * from MstOTIncentive where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and OTHrsFm=Convert(time,'" + from + "') and OTHrsTo=Convert(time,'" + To + "')";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtOTHrsFm.Text =dt.Rows[0]["OTHrsFm"].ToString();
            txtOTHrsTo.Text = dt.Rows[0]["OTHrsTo"].ToString();
            txtOTAmt.Text= dt.Rows[0]["OTAmt"].ToString();
            Load_OLD_Data();
            btnSave.Text = "Update";
        }
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string from = "";
        string To = "";

        from = e.CommandName.Split(',').FirstOrDefault();
        To = e.CommandName.Split(',').LastOrDefault();

        SSQL = "";
        SSQL = "Delete from MstOTIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'  and OTHrsFm=Convert(decimal(18,2),'" + from + "') and OTHrsTo=Convert(decimal(18,2),'" + To + "')";
        objdata.RptEmployeeMultipleDetails(SSQL);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "SaveMsgAlert('Details Deleted Successfully');", true);
        btnCancel_Click(sender, e);
        Load_OLD_Data();
    }
}