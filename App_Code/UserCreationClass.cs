﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for UserCreationClass
/// </summary>
public class UserCreationClass
{
	public UserCreationClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private string _UserCode;
    private string _UserName;
    private string _Password;
    private string _Ccode;
    private string _Lcode;
    private string _Data;

    private string _AttendanceLogClear;
    private string _ManualattendanceCheck;
    private string _ManAttnUpload;
    private string _EmpApproval;
    private string _EmpStatus;
    private string _Emp_Mst_Data_Perm;
    private int _UserID;

    public int UserID
    {
        get { return _UserID; }
        set { _UserID = value; }
    }

    public string UserCode
    {
        get { return _UserCode; }
        set { _UserCode = value; }
    }
    public string UserName
    {
        get { return _UserName; }
        set { _UserName = value; }

    }
    public string Password
    {
        get { return _Password; }
        set { _Password = value; }
    }
 
    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }
    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }

    public string Data
    {
        get { return _Data; }
        set { _Data = value; }
    }

    public string AttendanceLogClear
    {
        get { return _AttendanceLogClear; }
        set { _AttendanceLogClear = value; }
    }

    public string ManualattendanceCheck
    {
        get { return _ManualattendanceCheck; }
        set { _ManualattendanceCheck = value; }
    }

    public string ManAttnUpload
    {
        get { return _ManAttnUpload; }
        set { _ManAttnUpload = value; }
    }

    public string EmpApproval
    {
        get { return _EmpApproval; }
        set { _EmpApproval = value; }
    }
    public string EmpStatus
    {
        get { return _EmpStatus; }
        set { _EmpStatus = value; }
    }


    public string Emp_Mst_Data_Perm
    {
        get { return _Emp_Mst_Data_Perm; }
        set { _Emp_Mst_Data_Perm = value; }
    }

  }
