﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for SalaryMasterClass
/// </summary>
public class SalaryMasterClass
{
    private string _EmpNo;
    private string _ProfileType;
    private string _BasicSalary;
    private string _hraper;
    private string _hraamt;
    private string _conveyanceper;
    private string _conveyanceamt;
    private string _CCAper;
    private string _CCAamt;
    private string _medicalper;
    private string _medicalamt;
    private string _allowance1per;
    private string _allowance1amt;
    private string _allowance2per;
    private string _allowance2amt;
    private string _allowance3per;
    private string _allowance3amt;
    private string _netpay;
    private string _username;
    private string _dailySalary;
    private string _FinancePeriod;
    private string _Foodtype;
    private string _mess;
    private string _Hostel;
    private string _deduction1;
    private string _deduction2;
    private string _deduction3;
    private string _Labourallowance1;
    private string _Labourallowance2;
    private string _LabourAllowance3;
    private string _staffDeduction1;
    private string _staffDeduction2;
    private string _staffDeduction3;
    private string _staffDeduction1Per;
    private string _staffDeduction2Per;
    private string _staffDeduction3Per;
    private string _Ccode;
    private string _Lcode;
    private string _Base;
    private string _FDA;
    private string _HRA;
    private string _VDA;
    private string _Total;
    private string _Union;
    private string _EmpType;
    private string _PFsalary;
    public SalaryMasterClass()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string StaffDeduction1Per
    {
        get { return _staffDeduction1Per; }
        set { _staffDeduction1Per = value; }
    }
    public string StaffDeduction2Per
    {
        get { return _staffDeduction2Per; }
        set { _staffDeduction2Per = value; }
    }
    public string StaffDeduction3Per
    {
        get { return _staffDeduction3Per; }
        set { _staffDeduction3Per = value; }
    }
    public string StaffDecution1
    {
        get { return _staffDeduction1; }
        set { _staffDeduction1 = value; }
    }
    public string StaffDecution2
    {
        get { return _staffDeduction2; }
        set { _staffDeduction2 = value; }
    }
    public string StaffDecution3
    {
        get { return _staffDeduction3; }
        set { _staffDeduction3 = value; }
    }
    public string LabourAllowance1
    {
        get { return _Labourallowance1; }
        set { _Labourallowance1 = value; }
    }
    public string LabourAllowance2
    {
        get { return _Labourallowance2; }
        set { _Labourallowance2 = value; }
    }
    public string LabourAllowance3
    {
        get { return _LabourAllowance3; }
        set { _LabourAllowance3 = value; }
    }
    public string EmpNo
    {
        get { return _EmpNo; }
        set { _EmpNo = value; }
    }
    public string ProfileType
    {
        get { return _ProfileType; }
        set { _ProfileType = value; }
    }
    public string Basic
    {
        get { return _BasicSalary; }
        set { _BasicSalary = value; }
    }
    public string hraper
    {
        get { return _hraper; }
        set { _hraper = value; }
    }
    public string hraAmt
    {
        get { return _hraamt; }
        set { _hraamt = value; }
    }
    public string conveyanceper
    {
        get { return _conveyanceper; }
        set { _conveyanceper = value; }
    }
    public string conveyanceamt
    {
        get { return _conveyanceamt; }
        set { _conveyanceamt = value; }
    }
    public string CCAper
    {
        get { return _CCAper; }
        set { _CCAper = value; }
    }
    public string CCaamt
    {
        get { return _CCAamt; }
        set { _CCAamt = value; }
    }
    public string medicalper
    {
        get { return _medicalper; }
        set { _medicalper = value; }
    }
    public string medicalamt
    {
        get { return _medicalamt; }
        set { _medicalamt = value; }
    }
    public string Allowance1per
    {
        get { return _allowance1per; }
        set { _allowance1per = value; }
    }
    public string Allowance1Amt
    {
        get { return _allowance1amt; }
        set { _allowance1amt = value; }
    }
    public string Allowance2per
    {
        get { return _allowance2per; }
        set { _allowance2per = value; }
    }
    public string Allowance2Amt
    {
        get { return _allowance2amt; }
        set { _allowance2amt = value; }
    }
    public string Allowance3per
    {
        get { return _allowance3per; }
        set { _allowance3per = value; }
    }
    public string Allowance3Amt
    {
        get { return _allowance3amt; }
        set { _allowance3amt = value; }
    }
    public string netPay
    {
        get { return _netpay; }
        set { _netpay = value; }
    }
    public string username
    {
        get { return _username; }
        set { _username = value; }
    }
    public string dailySalary
    {
        get { return _dailySalary; }
        set { _dailySalary = value; }
    }
    public string Finance
    {
        get { return _FinancePeriod; }
        set { _FinancePeriod = value; }
    }
    public string FoodType
    {
        get { return _Foodtype; }
        set { _Foodtype = value; }
    }
    public string mess
    {
        get { return _mess; }
        set { _mess = value; }
    }
    public string Hostel
    {
        get { return _Hostel; }
        set { _Hostel = value; }
    }
    public string deduction1
    {
        get { return _deduction1; }
        set { _deduction1 = value; }
    }
    public string deduction2
    {
        get { return _deduction2; }
        set { _deduction2 = value; }
    }
    public string deduction3
    {
        get { return _deduction3; }
        set { _deduction3 = value; }
    }
    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }
    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }
    public string Base
    {
        get { return _Base; }
        set { _Base = value; }
    }
    public string FDA
    {
        get { return _FDA; }
        set { _FDA = value; }
    }
    public string HRA
    {
        get { return _HRA; }
        set { _HRA = value; }
    }
    public string VDA
    {
        get { return _VDA; }
        set { _VDA = value; }
    }
    public string total
    {
        get { return _Total; }
        set { _Total = value; }
    }
    public string union
    {
        get { return _Union; }
        set { _Union = value; }
    }
    public string EmpType
    {
        get { return _EmpType; }
        set { _EmpType = value; }
    }
    public string PFsalary
    {
        get { return _PFsalary; }
        set { _PFsalary = value; }
    }
}
