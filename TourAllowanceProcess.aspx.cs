﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;
using System.Data.SqlClient;

public partial class TourAllowanceProcess : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    static string CmpName;
    static string Cmpaddress;
    string SessionUserType;
    string SessionEpay;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SSQL = "";
    string SessionPayroll;
    bool ErrFlg = false;
    string ss = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();

        con = new SqlConnection(constr);

        if (!IsPostBack)
        {
            Load_DeptName();
        }
    }

    private void Load_DeptName()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDeptName.Items.Clear();
        query = "Select *from Department_Mst";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDeptName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DeptCode"] = "0";
        dr["DeptName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDeptName.DataTextField = "DeptName";
        ddlDeptName.DataValueField = "DeptCode";
        ddlDeptName.DataBind();
    }
    protected void btnProcess_Click(object sender, EventArgs e)
    {
        if (ddlCategory.SelectedValue == "0")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Category')", true);
        }
        if (ddlEmployeeType.SelectedValue == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Employee Type')", true);
        }
        
        if(txtFromDate.Text=="" || txtToDate.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the From Date and To Date Properly')", true);
        }
        if (!ErrFlg)
        {

            SSQL = "";
            SSQL = "Delete from [" + SessionEpay + "]..TourAllowanceProcess where ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and CateCode='"+ddlCategory.SelectedValue+"'";
            SSQL = SSQL + " and EmpTypeCode='" + ddlEmployeeType.SelectedValue + "' and DeptCode='" + ddlDeptName.SelectedValue + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Insert into [" + SessionEpay + "]..TourAllowanceProcess(Ccode,Lcode,MachineID,EmpName,TotalWorkedDays,Amount,Deduction,FinalAmt,CateCode,CateName,EmpTypeCode,EmpTypeName,FromDate,ToDate,deptCode,deptname) ";
            SSQL = SSQL + " Select '"+SessionCcode+"' as Ccode,'"+SessionLcode+"' as Lcode,EM.MachineID,EM.FirstName,isnull(sum(AT.Days), 0) as WorkedDays,isnull(sum(AT.Days), 0) * isnull(TA.Allowance, 0) as Amt,isnull(TA.Deduction, 0) as Deduction,";
            SSQL = SSQL + "Cast((isnull(sum(AT.Days), 0) * isnull(TA.Allowance, 0)) - isnull(TA.Deduction, 0) as decimal(18, 0)) as FinalAmt,'"+ddlCategory.SelectedValue+"' as CateCode,";
            SSQL = SSQL + "'" + ddlCategory.SelectedItem.Text + "' as CateName,'" + ddlEmployeeType.SelectedValue + "' as EmpTypeCode,'" + ddlEmployeeType.SelectedItem.Text + "' as EmpTypeName,'"+txtFromDate.Text+"' as FromDate,'"+txtToDate.Text+"' as ToDate,'"+ddlDeptName.SelectedValue+"' as deptcode,'"+ddlDeptName.SelectedItem.Text+"' as deptName ";
            SSQL = SSQL + " from Employee_Mst EM inner join [" + SessionEpay + "]..AttenanceDetails AT on AT.EmpNo=EM.EmpNo and AT.Ccode=EM.CompCode and AT.Lcode=EM.LocCode";
            SSQL = SSQL + " inner join [" + SessionEpay + "]..MstTourAllowance TA on TA.CateCode='" + ddlCategory.SelectedValue + "' and TA.EmpTypeCode='" + ddlEmployeeType.SelectedValue + "'";
            SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "' and EM.CatName='" + ddlCategory.SelectedItem.Text + "' and EM.Wages='" + ddlEmployeeType.SelectedItem.Text + "' and ";
            SSQL = SSQL + " cast(EM.BaseSalary as decimal(18,2))>=cast(TA.MinWages as decimal(18,2)) and cast(EM.BaseSalary as decimal(18,2))<=cast(TA.MaxWages as decimal(18,2))";
            SSQL = SSQL + " and TA.CateCode='" + ddlCategory.SelectedValue + "' and TA.EmpTypeCode='" + ddlEmployeeType.SelectedValue + "' and EM.IsActive='Yes' and Convert(datetime,AT.FromDate,103)>=convert(datetime,'"+txtFromDate.Text+ "',103)";
            SSQL = SSQL + " and Convert(datetime,ToDate,103)<=convert(datetime,'" + txtToDate.Text + "',103) and TA.Ccode='" + SessionCcode + "' and TA.Lcode='" + SessionLcode + "' ";
            if (ddlDeptName.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " and EM.DeptCode='" + ddlDeptName.SelectedValue + "'";
            }
            SSQL = SSQL + " group by EM.MachineID,EM.FirstName,TA.Allowance,TA.Deduction order by EM.MachineID";
            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Process Completed Successfully')", true);
            btnClr_Click(sender, e);
        }
    }

    protected void btnClr_Click(object sender, EventArgs e)
    {
        ddlCategory.ClearSelection();
        ddlEmployeeType.ClearSelection();
        ddlDeptName.ClearSelection();
        txtFromDate.Text = "";
        txtToDate.Text = "";
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        ddlEmployeeType.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        ddlEmployeeType.Items.Insert(0, new ListItem("-Select-", "-Select-"));
    }
}