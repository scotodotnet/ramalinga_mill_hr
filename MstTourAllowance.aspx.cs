﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;
using System.Data.SqlClient;

public partial class MstTourAllowance : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    static string CmpName;
    static string Cmpaddress;
    string SessionUserType;
    string SessionEpay;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SSQL = "";
    string SessionPayroll;
    bool ErrFlg = false;
    string ss = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();

        con = new SqlConnection(constr);

        if (!IsPostBack)
        {
            Load_DeptName();
        }

        Load_Data();
    }

    private void Load_DeptName()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDeptName.Items.Clear();
        query = "Select *from Department_Mst";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDeptName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DeptCode"] = "0";
        dr["DeptName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDeptName.DataTextField = "DeptName";
        ddlDeptName.DataValueField = "DeptCode";
        ddlDeptName.DataBind();
    }

    private void Load_Data()
    {
        SSQL = "Select * from [" + SessionEpay + "]..MstTourAllowance where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
       
        if (ddlCategory.SelectedValue == "0")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Category')", true);
        }
        if (ddlEmployeeType.SelectedValue == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Select the Employee Type')", true);
        }
        if (txtMinimumWages.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Minimum Wages Amt')", true);
        }
        if (txtMaximumWages.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Maxmum Wages Amt')", true);
        }
        if (txtAllowance.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Allowance')", true);
        }
        if (txtDeduction.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Deduction')", true);
        }
        if (!ErrFlg)
        {
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from [" + SessionEpay + "]..MstTourAllowance where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and CateCode='" + ddlCategory.SelectedValue + "' and EmpTypeCode='" + ddlEmployeeType.SelectedValue + "'";
                SSQL = SSQL + " and MinWages>='" + txtMinimumWages.Text + "' and '" + txtMaximumWages.Text + "'<=MaxWages";
                if (ddlDeptName.SelectedValue != "-Select-")
                {
                    SSQL = SSQL + " and DeptCode='" + ddlDeptName.SelectedValue + "'";
                }
                
                DataTable dt_check = new DataTable();
                dt_check = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_check != null && dt_check.Rows.Count > 0)
                {
                    ErrFlg = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('This Wages is Already Taken')", true);
                }
            }
            if (!ErrFlg)
            {
                SSQL = "";
                SSQL = "delete from [" + SessionEpay + "]..MstTourAllowance where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and CateCode='" + ddlCategory.SelectedValue + "' and EmpTypeCode='" + ddlEmployeeType.SelectedValue + "'";
                SSQL = SSQL + " and MinWages>='" + txtMinimumWages.Text + "' and MaxWages <= '" + txtMaximumWages.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "";
                SSQL = "Insert into [" + SessionEpay + "]..MstTourAllowance(Ccode,Lcode,CateCode,CateName,EmpTypeCode,EmpTypeName,DeptCode,DeptName,MinWages,MaxWages,Allowance,Deduction)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + ddlCategory.SelectedValue + "','" + ddlCategory.SelectedItem.Text + "'";
                SSQL = SSQL + ", '" + ddlEmployeeType.SelectedValue + "','" + ddlEmployeeType.SelectedItem.Text + "','" + ddlDeptName.SelectedValue + "','"+ddlDeptName.SelectedItem.Text+"','" + txtMinimumWages.Text + "','" + txtMaximumWages.Text + "','" + txtAllowance.Text + "','" + txtDeduction.Text + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                if (btnSave.Text == "Save")
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Tour Allowance Saved Successfully')", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Tour Allowance Updated Successfully')", true);
                }

                btnClr_Click(sender, e);
            }
            
        }
    }

    protected void btnClr_Click(object sender, EventArgs e)
    {
        ddlCategory.ClearSelection();
        ddlEmployeeType.ClearSelection();
        ddlDeptName.ClearSelection();
        txtAllowance.Text = "0.00";
        txtDeduction.Text = "0.00";
        txtMaximumWages.Text = "0.00";
        txtMinimumWages.Text = "0.00";
        btnSave.Text = "Save";
        Load_Data();    
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        ddlEmployeeType.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        ddlEmployeeType.Items.Insert(0, new ListItem("-Select-", "-Select-"));

    }

    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string EmpTypeCode = "";
        string CateCode = "";
        string MinWages = "0";
        string MaxWages = "0";

        string[] Array = e.CommandName.Split(',');

        CateCode = Array[0].ToString();
        EmpTypeCode = Array[1].ToString();

        string[] Array2 = e.CommandArgument.ToString().Split(',');

        MinWages = Array2[0];
        MaxWages = Array2[1];

        DataTable dt = new DataTable();
        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstTourAllowance where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        SSQL = SSQL + " and MinWages='" + MinWages + "' and MaxWages='" + MaxWages + "'";
        SSQL = SSQL + " and CateCode='" + CateCode + "' and EmpTypeCode='" + EmpTypeCode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt != null && dt.Rows.Count > 0)
        {
            ddlCategory.SelectedValue = dt.Rows[0]["CateCode"].ToString();
            ddlCategory_SelectedIndexChanged(sender, e);
            ddlEmployeeType.SelectedValue = dt.Rows[0]["EmpTypeCode"].ToString();
            //ddlFinance.SelectedValue = dt.Rows[0]["Year"].ToString();
            txtMinimumWages.Text = dt.Rows[0]["MinWages"].ToString();
            txtMaximumWages.Text = dt.Rows[0]["MaxWages"].ToString();
            txtAllowance.Text = dt.Rows[0]["Allowance"].ToString();
            txtDeduction.Text = dt.Rows[0]["Deduction"].ToString();
            btnSave.Text = "Update";
        }
        Load_Data();

    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string EmpTypeCode = "";
        string CateCode = "";
        string MinWages = "0";
        string MaxWages = "0";

        string[] Array = e.CommandName.Split(',');

        CateCode = Array[0].ToString();
        EmpTypeCode = Array[1].ToString();

        string[] Array2 = e.CommandArgument.ToString().Split(',');

        MinWages = Array2[0];
        MaxWages = Array2[1];
        
        SSQL = "";
        SSQL = "Delete from [" + SessionEpay + "]..MstTourAllowance where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        SSQL = SSQL + " and MinWages='" + MinWages + "' and MaxWages='" + MaxWages + "'";
        SSQL = SSQL + " and CateCode='" + CateCode + "' and EmpTypeCode='" + EmpTypeCode + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data();
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Tour Allowance Deleted Successfully')", true);
       
    }
}