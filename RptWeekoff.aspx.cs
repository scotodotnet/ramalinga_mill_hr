﻿using System;
using System.Data;
using System.Web.UI;
using Altius.BusinessAccessLayer.BALDataAccess;
using CrystalDecisions.CrystalReports.Engine;
public partial class RptWeekoff : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    string OT_Eligible = "";
    string Wages_Type = "";
    string OT_Above_Eight = "0";
    string OT_Above_Four = "0";
    string OT_Below_Four = "0";
    string OT_Total_Hours = "0";
    string Weekly_Amt = "0";
    ReportDocument report = new ReportDocument();
    string OT_Hours = "0";
    DataTable Log_DS = new DataTable();
    DataTable dt_OT = new DataTable();
    DataTable dt_OT_Eligible = new DataTable();
    string SessionPayroll;
    BALDataAccess objdata = new BALDataAccess();

    string Cals_Amt = "0";

    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DataTable mDataSet = new DataTable();
    DataTable NFH_DS = new DataTable();
    DataTable NFH_Type_Ds = new DataTable();
    DataTable WH_DS = new DataTable();
    string CmpName, Cmpaddress;
    DateTime NFH_Date = new DateTime();
    DateTime DOJ_Date_Format = new DateTime();
    string qry_nfh = "";
    string SSQL = "";
    DateTime Week_Off_Date = new DateTime();
    DateTime WH_DOJ_Date_Format = new DateTime();
    Boolean Check_Week_Off_DOJ = false;
    double NFH_Present_Check = 0;
    decimal NFH_Days_Count = 0;
    decimal AEH_NFH_Days_Count = 0;
    decimal LBH_NFH_Days_Count = 0;
    decimal NFH_Days_Present_Count = 0;
    decimal WH_Count = 0;
    decimal WH_Present_Count = 0;
    decimal Present_Days_Count = 0;
    int Fixed_Work_Days = 0;
    decimal Spinning_Incentive_Days = 0;
    string Total_Amt = "";
    decimal NFH_Double_Wages_Checklist = 0;
    decimal NFH_Double_Wages_Statutory = 0;
    decimal NFH_Double_Wages_Manual = 0;
    decimal NFH_WH_Days_Mins = 0;
    decimal NFH_Single_Wages = 0;
    int Month_Mid_Total_Days_Count;
    string NFH_Type_Str = "";
    string NFH_Name_Get_Str = "";
    string NFH_Dbl_Wages_Statutory_Check = "";
    string Emp_WH_Day = "";
    string DOJ_Date_Str = "";
    string Week_Off = "";

    string NFH_Date_P_Date = "";
    DateTime date1;
    DateTime date2;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    string WagesValue;
    string oldWages;
    DataSet ds = new DataSet();
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();

    DataTable dt_Cat = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | WEEKOFF REPORT ";

                Load_DB();

            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();


            if (SessionUserType == "2")
            {
                NonAdminPayrollAttn();
            }

            else
            {

                PayrollAttn();

            }


            if (AutoDTable.Rows.Count > 0)
            {
                SSQL = "";

                SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                
                dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt_Cat.Rows.Count > 0)
                {
                    CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
                }

                grid.DataSource = AutoDTable;
                grid.DataBind();
                string attachment = "attachment;filename=Week_Off.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='5'>");
                Response.Write("<a style=\"font-weight:bold\">" + CmpName + "</a>");
                Response.Write("--");
                Response.Write("<a style=\"font-weight:bold\">" + Cmpaddress + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='5'>");
                Response.Write("<a style=\"font-weight:bold\">WEEK-OFF REPORT - " + FromDate + "</a>");

                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.Write("<table border=1>");
                Response.Write("<tr>");
                Response.Write("<td colspan=7 align='center' style=\"font-weight:bold\"> TOTAL");
                Response.Write("</td> ");
                Response.Write("<td> =sum(H4:H" + Convert.ToInt32(AutoDTable.Rows.Count + 3) + ")");
                Response.Write("</td>");
              
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.End();
                Response.Clear();

                //ds.Tables.Add(AutoDTable);
                //report.Load(Server.MapPath("crystal/WeekOff_New.rpt"));
                //SSQL = "";
                //SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                ////dt = objdata.RptEmployeeMultipleDetails(SSQL);
                //dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);

                //if (dt_Cat.Rows.Count > 0)
                //{
                //    CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                //    Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
                //}

                //report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                //report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName.ToString() + "'";
                //report.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
                //report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate + "'";
                //report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate + "'";
                //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                //CrystalReportViewer1.ReportSource = report;
            }
        }
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Ramalinga_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();

            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    public void PayrollAttn()
    {
        AutoDTable.Columns.Add("Sno");
        AutoDTable.Columns.Add("Code");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("AccountNo");
        //AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("Wages");
        AutoDTable.Columns.Add("TimeIN");
        AutoDTable.Columns.Add("TimeOUT");
        AutoDTable.Columns.Add("Amount");

        try
        {
            int intI;
            SSQL = "";
            SSQL = "Select LTD.MachineID,LTD.ExistingCode,LTD.TimeIN,LTD.TimeOUT,LTD.Shift,EM.FirstName,sum(LTD.Present) as Days,LTD.Total_Hrs,";
            SSQL = SSQL + " EM.DOJ,EM.Weekoff,EM.OT_Salary,EM.DeptName,EM.Wages,EM.AccountNo,LTD.Total_Hrs1 from LogTime_Days LTD inner join Employee_Mst EM on";
            SSQL = SSQL + " EM.CompCode=LTD.CompCode And EM.LocCode=LTD.LocCode And LTD.MachineID=EM.MachineID";
            SSQL = SSQL + " where LTD.CompCode='" + SessionCcode + "' And LTD.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103))";
            SSQL = SSQL + " and CONVERT(DATETIME,LTD.Attn_Date_Str,103) = CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
            
            SSQL = SSQL + " And LTD.Present!='0.0' and CatName not like '%STAFF%' and upper(EM.WeekOff) not like '%NONE%' and EM.WeekOffEligible='1'";

            SSQL = SSQL + " Group by LTD.MachineID,LTD.ExistingCode,LTD.TimeIN,LTD.TimeOUT,LTD.Shift,EM.FirstName,EM.DOJ,EM.Weekoff,EM.OT_Salary,EM.DeptName,EM.Wages,EM.AccountNo,LTD.Total_Hrs,LTD.Total_Hrs1 ";
            SSQL = SSQL + " Order by cast(LTD.ExistingCode as int) Asc";

            Log_DS = objdata.RptEmployeeMultipleDetails(SSQL);

            intI = 2;

            for (int i = 0; i < Log_DS.Rows.Count; i++)
            {
                string Machineid = Log_DS.Rows[i]["MachineID"].ToString();

                if (Machineid == "930056")
                {
                    string StopQuery = "";
                }
           
                Emp_WH_Day = "";
                DOJ_Date_Str = "";

                DOJ_Date_Str = Log_DS.Rows[i]["DOJ"].ToString();
                Emp_WH_Day = Log_DS.Rows[i]["WeekOff"].ToString();
              
                //'Week of Check

                Check_Week_Off_DOJ = false;

                SSQL = "Select Present,Attn_Date from LogTime_Days where MachineID='" + Log_DS.Rows[i]["MachineID"] + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And upper(DATENAME(weekday,Attn_Date))=upper('" + Emp_WH_Day + "')";
                SSQL = SSQL + " And Attn_Date = CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";

               // SSQL = SSQL + " And Attn_Date <=CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";

                WH_DS = objdata.RptEmployeeMultipleDetails(SSQL);

                if (WH_DS.Rows.Count > 0)
                {
                    Week_Off_Date = Convert.ToDateTime(WH_DS.Rows[0]["Attn_Date"]);

                    WH_DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                    if (WH_DOJ_Date_Format.Date <= Week_Off_Date.Date)
                    {
                        Check_Week_Off_DOJ = true;
                    }
                    else
                    {
                        Check_Week_Off_DOJ = false;
                    }

                    if (Check_Week_Off_DOJ == true)
                    {
                        WH_Count = WH_Count + 1;
                        WH_Present_Count = Convert.ToDecimal(Log_DS.Rows[i]["Total_Hrs"].ToString());
                    }
                }

                //'Total Days Get

                if (WH_Present_Count > 0)
                {
                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();
                    //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["S.No"] = AutoDTable.Rows.Count + 1;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Sno"] = AutoDTable.Rows.Count;
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Code"] = Log_DS.Rows[i]["MachineID"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["AccountNo"] = Log_DS.Rows[i]["AccountNo"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Name"] = Log_DS.Rows[i]["FirstName"].ToString();
                    //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Dept"] = Log_DS.Rows[i]["DeptName"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Wages"] = Log_DS.Rows[i]["Wages"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeIN"] = Log_DS.Rows[i]["TimeIN"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeOUT"] = Log_DS.Rows[i]["TimeOUT"].ToString();

                    if(Log_DS.Rows[i]["MachineID"].ToString()=="030148")
                    {
                        string Pause = "";
                    }

                    decimal Inc_Amt = 0;
                    SSQL = "";
                    SSQL = "Select  isnull(Amt,0) as Amt from MstWeekOFFIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    DataTable inc_dt = new DataTable();
                    inc_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (inc_dt != null && inc_dt.Rows.Count > 0)
                    {
                        Inc_Amt = Convert.ToDecimal(inc_dt.Rows[0]["Amt"]);
                    }
                    else
                    {
                        Inc_Amt = 0;
                    }

                    Weekly_Amt = (Convert.ToDecimal(Log_DS.Rows[i]["OT_Salary"].ToString()) + Convert.ToDecimal(Inc_Amt)).ToString();

                    //Cals_Amt = ((Convert.ToDecimal(Weekly_Amt)) / Convert.ToDecimal(8)).ToString();


                    if (Log_DS.Rows[i]["Wages"].ToString() == "STAFF")
                    {
                        if (Convert.ToDecimal(WH_Present_Count) >= 8)
                        {
                           
                            Total_Amt = (Convert.ToDecimal(Weekly_Amt) / Convert.ToDecimal(26)).ToString();
                            Total_Amt = (Convert.ToDecimal(Total_Amt)).ToString();

                           // Total_Amt = (Math.Round(Convert.ToDecimal(Total_Amt), 0, MidpointRounding.AwayFromZero)).ToString();
                        }
                        else
                        {
                            Total_Amt = (Convert.ToDecimal(Weekly_Amt) / Convert.ToDecimal(26)).ToString();
                            Total_Amt = (Convert.ToDecimal(Total_Amt) / Convert.ToDecimal(8)).ToString();
                            Total_Amt = (Convert.ToDecimal(Total_Amt) * Convert.ToDecimal(WH_Present_Count)).ToString();

                           // Total_Amt = (Math.Round(Convert.ToDecimal(Total_Amt), 0, MidpointRounding.AwayFromZero)).ToString();
                        }
                    }
                    else
                    {
                        Cals_Amt = ((Convert.ToDecimal(Weekly_Amt)) / Convert.ToDecimal(8)).ToString();

                        if (Convert.ToDecimal(WH_Present_Count) >= 8)
                        {
                            Total_Amt = (Convert.ToDecimal(Cals_Amt) * Convert.ToDecimal(8)).ToString();

                            //Total_Amt = (Math.Round(Convert.ToDecimal(Total_Amt), 0, MidpointRounding.AwayFromZero)).ToString();
                        }
                        else
                        {
                            if (Log_DS.Rows[i]["Shift"].ToString().ToUpper() == "SHIFT3" && Convert.ToDecimal(TimeSpan.Parse(Log_DS.Rows[i]["Total_Hrs1"].ToString()).TotalHours) >=Convert.ToDecimal(TimeSpan.Parse("06:30").TotalHours))
                            {
                                Total_Amt = (Convert.ToDecimal(Cals_Amt) * Convert.ToDecimal(8)).ToString();

                               // Total_Amt = (Math.Round(Convert.ToDecimal(Total_Amt), 0, MidpointRounding.AwayFromZero)).ToString();
                            }
                            else
                            {
                                Total_Amt = (Convert.ToDecimal(Cals_Amt) * Convert.ToDecimal(WH_Present_Count)).ToString();

                             //   Total_Amt = (Math.Round(Convert.ToDecimal(Total_Amt), 0, MidpointRounding.AwayFromZero)).ToString();
                            }
                        }
                    }
                    string[] decimal_point_check = Total_Amt.ToString().Split('.');
                    if (decimal_point_check.Length > 1 && Convert.ToInt32(decimal_point_check[1]) > 0)
                    {
                        Total_Amt = (Convert.ToInt32(decimal_point_check[0]) + 1).ToString();
                    }
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Amount"] = Total_Amt;

                    intI = intI + 1;
                }

                WH_Present_Count = 0;
                NFH_Days_Count = 0;
                AEH_NFH_Days_Count = 0;
                LBH_NFH_Days_Count = 0;
                NFH_Double_Wages_Checklist = 0;
                NFH_Double_Wages_Statutory = 0;
                NFH_Double_Wages_Manual = 0;
                NFH_WH_Days_Mins = 0;

            }
        }
        catch (Exception ex)
        {

        }
    }
  
    public void NonAdminPayrollAttn()
    { 
        
    }
}
