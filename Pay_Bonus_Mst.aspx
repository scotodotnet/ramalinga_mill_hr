﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="Pay_Bonus_Mst.aspx.cs" Inherits="Pay_Bonus_Mst" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
  <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable({
             "bPaginate": false
         });
     });
	</script>
	<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable({
                    "bPaginate": false
                });
            }
        });
    };
</script>
<!-- begin #content -->
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
	    <li class="active">Bonus Master Details</li>
	</ol>
	<h1 class="page-header">BONUS MASTER DETAILS</h1>
	<div class="row">
        <div class="col-md-12">
		    <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Bonus Master Details</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <%--<h5>STAFF AND REGULAR WORKER BONUS FIXED DETAILS</h5>--%>
                        <div class="row">
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Category</label>
								    <asp:DropDownList runat="server" ID="ddlCategory" class="form-control select2" AutoPostBack="true"
                                        onselectedindexchanged="ddlCategory_SelectedIndexChanged">
								        <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
								       <asp:ListItem Text="STAFF" Value="1"></asp:ListItem>
								       <asp:ListItem Text="LABOUR" Value="2"></asp:ListItem>
								    </asp:DropDownList>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group">
								    <label>Employee Type</label>
								    <asp:DropDownList runat="server" ID="ddlEmployeeType" class="form-control select2" style="width:100%;" AutoPostBack="true"
                                        onselectedindexchanged="ddlEmployeeType_SelectedIndexChanged">
							 	    </asp:DropDownList>
								</div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-md-2">
							    <div class="form-group">
								    <label>From Year</label>
								    <asp:TextBox runat="server" ID="txtbelowoneYear" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                             <div class="col-md-2">
							    <div class="form-group">
								    <label>To Year</label>
								    <asp:TextBox runat="server" ID="txtAboveOneYear" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3" runat="server" visible="false">
							    <div class="form-group">
								    <label>Gross Salary %</label>
								    <asp:TextBox runat="server" ID="txtGrossSalPercent" Text="100.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-2">
							    <div class="form-group">
								    <label>Bonus %</label>
								    <asp:TextBox runat="server" ID="txtBonusPercent" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Type</label>
								
								    <asp:RadioButtonList ID="RdbType" runat="server" RepeatColumns="3" class="form-control">
                                          <asp:ListItem Selected="true" Text="Year" style="padding-right:40px" Value="1"></asp:ListItem>
                                          <asp:ListItem Selected="False" Text="Month" style="padding-right:40px" Value="2"></asp:ListItem>
                                     </asp:RadioButtonList>
								 </div>
                               </div>
                           <!-- end col-4 -->
                            <div class="col-md-3">
							    <div class="form-group"  runat="server" visible="false">
								    <label>Above Days %</label>
								    <asp:TextBox runat="server" ID="txtAboveDays" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group"  runat="server" visible="false">
								    <label>Below Days %</label>
								    <asp:TextBox runat="server" ID="txtBelowDays" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                           
                            <div class="col-md-3">
							    <div class="form-group"  runat="server" visible="false">
								    <label>Min. Amount</label>
								    <asp:TextBox runat="server" ID="txtMinAmount" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-4">
								<div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave1" Text="Save" class="btn btn-success" onclick="btnSave1_Click" />
									<asp:Button runat="server" id="btnClear1" Text="Clear" class="btn btn-danger" onclick="btnClear1_Click" />
						    	</div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="row">
                                        <!-- table start -->
                                        <div class="col-md-12">
                                            <div class="row">
                                                <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                    <HeaderTemplate>
                                                        <table id="example" class="display table">
                                                            <thead>
                                                                <tr>
                                                                    <th>S.No</th>
                                                                    <th>Category</th>
                                                                    <th>EmpType</th>
                                                                   <th>From Year</th>
                                                                   <th>To Year</th>
                                                                   <%--<th>Gross Sal %</th>--%>
                                                                   <th>Bonus %</th>
                                                                    <%--<th>Fin Year</th>--%>
                                                                    <th>Mode</th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex + 1 %></td>
                                                            <td><%# Eval("CategoryName")%></td>
                                                            <td><%# Eval("EmpType")%></td>
                                                            <td><%# Eval("BelowOneYear")%></td>
                                                            <td><%# Eval("AboveOneYear")%></td>
                                                            <%--<td><%# Eval("Gross_Sal_Percent")%></td>--%>
                                                            <td><%# Eval("Bonus_Percent")%></td>
                                                            <%--<td><%# Eval("BelowOneYear")%></td>--%>
                                                            <td>
                                                                <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil" runat="server"
                                                                    Text="" OnCommand="btnEditEnquiry_Grid_Command" CommandArgument='<%# Eval("BelowOneYear")+","+Eval("AboveOneYear")%>' CommandName='<%# Eval("Category")+","+Eval("EmployeeType")%>'>
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                    Text="" OnCommand="btnDeleteEnquiry_Grid_Command" CommandArgument='<%# Eval("BelowOneYear")+","+Eval("AboveOneYear")%>' CommandName='<%# Eval("Category")+","+Eval("EmployeeType")%>'
                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Bonus details?');">
                                                                </asp:LinkButton>
                                                            </td>

                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                        <!-- table End -->
                                    </div>
                         <%--<div class="panel-body">
                        <h5>HOSTEL WORKER BONUS FIXED DETAILS</h5>
                        <div class="row">
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Below 1 Year Days</label>
								    <asp:TextBox runat="server" ID="TextBox7" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Above Days</label>
								    <asp:TextBox runat="server" ID="TextBox8" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Below Days</label>
								    <asp:TextBox runat="server" ID="TextBox9" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Above 1 Year And Below 2 Year</label>
								    <asp:TextBox runat="server" ID="TextBox10" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Above 2 Year And Below 3 Year</label>
								    <asp:TextBox runat="server" ID="TextBox11" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Above 3 Year</label>
								    <asp:TextBox runat="server" ID="TextBox12" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                            <div class="col-md-3">
							    <div class="form-group">
								    <label>Min. Amount</label>
								    <asp:TextBox runat="server" ID="TextBox13" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-4">
								<div class="form-group">
									<br />
									<asp:Button runat="server" id="Button1" Text="Save" class="btn btn-success" />
									<asp:Button runat="server" id="Button2" Text="Clear" class="btn btn-danger" />
						    	</div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        </div>--%>
                        <asp:Panel runat="server" Visible="false" ID="EL_panel">
                        <h5>EL Percentage Fixed</h5>
                        <div class="row">
                            <div class="col-md-4">
								<div class="form-group">
								    <label>EL Year</label>
								    <asp:DropDownList runat="server" ID="txtELYear" class="form-control select2" style="width:100%;" AutoPostBack="true"
                                        onselectedindexchanged="txtELYear_SelectedIndexChanged">
							 	    </asp:DropDownList>
								</div>
                            </div>
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>EL Percentage</label>
								    <asp:TextBox runat="server" ID="txtEL_Percentage" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-4">
								<div class="form-group">
									<br />
									<asp:Button runat="server" id="btnELPercentSave" Text="Save" class="btn btn-success" onclick="btnELPercentSave_Click" />
									
						    	</div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                            </asp:Panel>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</asp:Content>

