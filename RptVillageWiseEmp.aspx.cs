﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class RptVillageWiseEmp : System.Web.UI.Page
{

    string Status = "";
    string SSQL = "";
    string SessionPayroll;

    BALDataAccess objdata = new BALDataAccess();
    DataTable dt = new DataTable();
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    ReportDocument rd = new ReportDocument();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    string VillageName = "";
    string TokenNo = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Village Wise Employee Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            //();

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            // ReportName = Request.QueryString["Report"].ToString();
            Status = Request.QueryString["Status"].ToString();
            VillageName = Request.QueryString["Village"].ToString();
            TokenNo = Request.QueryString["TokenNo"].ToString();
            SessionPayroll = Session["SessionEpay"].ToString();

            string table = "";
            if (Status == "Pending")
            {
                table = "Employee_Mst_New_Emp";
            }
            else
            {
                table = "Employee_Mst";
            }

            SSQL = "";
            SSQL = "Select EmpNo,FirstName,Temp_Village from " + table + " where Compcode='" + SessionCcode + "' and Loccode='" + SessionLcode + "'";
            if (VillageName != "-Select-")
            {
                SSQL = SSQL + " and Temp_Village='" + VillageName + "'";
            }
            if (TokenNo.Trim() != "")
            {
                SSQL = SSQL + " and EmpNo='" + TokenNo.Trim() + "'";
            }
            SSQL = SSQL + " Order by EmpNo,Temp_Village ASC";

            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {

                string CmpName = "";
                string Cmpaddress = "";
                DataTable dt_Cat = new DataTable();

                SSQL = "";
                SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
                }

                rd.Load(Server.MapPath("crystal/VillageWiseEmp.rpt"));
                rd.SetDataSource(dt);

                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";

                rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.RefreshReport();
                CrystalReportViewer1.DataBind();

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
            }
        }
    }
}