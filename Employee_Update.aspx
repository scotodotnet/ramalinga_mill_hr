﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Employee_Update.aspx.cs" Inherits="Employee_Update" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
    </script>
     <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
     });
	</script>


<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
            }
        });
    };
</script>
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            swal(msg);
        }
    </script>

    <script type="text/javascript">
        function ProgressBarShow() {
            $('#Download_loader').show();
        }
    </script>

    <script type="text/javascript">
        function ProgressBarHide() {
            $('#Download_loader').hide();
        }
    </script>
    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="javascript:;">Employee Update</a></li>
            <%--<li class="active">Salary Calculation</li>--%>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Employee Update</h1>
        <!-- end page-header -->

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <asp:UpdatePanel ID="SalPay" runat="server">
                <ContentTemplate>
                    <div class="col-md-12">
                        <!-- begin panel -->
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">Employee Update</h4>
                            </div>
                            <div class="panel-body">
                                <asp:UpdatePanel runat="server"> <ContentTemplate>
                                <fieldset>
                                    <h5><b>Week OFF Update</b></h5>
                                </fieldset>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Wages</label>
                                                <asp:DropDownList ID="ddlEmployeeType" CssClass="form-control select2" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Week OFF Employees From</label>
                                                <asp:DropDownList ID="ddlWeekoffFrom" runat="server" class="form-control BorderStyle select2" Width="100%">
                                                    <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                    <asp:ListItem Value="Sunday" Text="Sunday"></asp:ListItem>
                                                    <asp:ListItem Value="Monday" Text="Monday"></asp:ListItem>
                                                    <asp:ListItem Value="Tuesday" Text="Tuesday"></asp:ListItem>
                                                    <asp:ListItem Value="Wednesday" Text="Wednesday"></asp:ListItem>
                                                    <asp:ListItem Value="Thursday" Text="Thursday"></asp:ListItem>
                                                    <asp:ListItem Value="Friday" Text="Friday"></asp:ListItem>
                                                    <asp:ListItem Value="Saturday" Text="Saturday"></asp:ListItem>
                                                    <asp:ListItem Value="NONE" Text="NONE"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Week OFF Employees To</label>
                                                <asp:DropDownList ID="ddlWeekOffTo" runat="server" class="form-control BorderStyle select2" Width="100%">
                                                    <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                    <asp:ListItem Value="Sunday" Text="Sunday"></asp:ListItem>
                                                    <asp:ListItem Value="Monday" Text="Monday"></asp:ListItem>
                                                    <asp:ListItem Value="Tuesday" Text="Tuesday"></asp:ListItem>
                                                    <asp:ListItem Value="Wednesday" Text="Wednesday"></asp:ListItem>
                                                    <asp:ListItem Value="Thursday" Text="Thursday"></asp:ListItem>
                                                    <asp:ListItem Value="Friday" Text="Friday"></asp:ListItem>
                                                    <asp:ListItem Value="Saturday" Text="Saturday"></asp:ListItem>
                                                    <asp:ListItem Value="NONE" Text="NONE"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>

                                <!-- begin row -->
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <!-- begin col-4 -->
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <br />
                                            <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success"
                                                OnClick="btnSave_Click" />

                                            <asp:Button ID="btnCancel" runat="server" class="btn btn-warning"
                                                Text="Cancel" OnClick="btnCancel_Click" />
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                    <div class="col-md-4"></div>
                                </div>

                                </div>
                                    </ContentTemplate></asp:UpdatePanel>
                                <div id="Download_loader" style="display: none" />
                            </div>
                        </div>
                        <!-- end panel -->
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSave" />
                    <asp:PostBackTrigger ControlID="btnCancel" />
                </Triggers>
            </asp:UpdatePanel>
            <!-- end col-12 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end #content -->
</asp:Content>
