<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MDFineReport.aspx.cs" Inherits="MDFineReport" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<style type="text/css">
        .CalendarCSS
        {
            background-color:White;
            color:Black;
            border:1px solid #646464;
            font-size:xx-large;
            font-weight: bold;
            margin-bottom: 40px;
            margin-top: 20px;
        }
</style>

<script type="text/javascript">
        $(document).ready(function () {
       
            $("ul[id*=myid] li").click(function () {
                alert($(this).html()); // gets innerHTML of clicked li
                alert($(this).text()); // gets text contents of clicked li
               
            });
        });
</script>

 <script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });

                $('.select2').select2();
            }
        });
    };
</script>

<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>

<asp:UpdatePanel ID="UpdatePanel5" runat="server">
 <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">MD Report</a></li>
				<li class="active">Unitwise Fine Report</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Unitwise Fine Report</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Unitwise Fine Report</h4>
                        </div>
                        <div class="panel-body">
                           <div class="row">
							 <div class="form-group col-md-4">
							     <label>Unit</label>
								 <asp:DropDownList runat="server" ID="ddlunit" class="form-control select2">
								 </asp:DropDownList>
							 </div>
                             
                           <div class="col-md-4">
								 <div class="form-group">
								
									 <label>Month</label>
								 <asp:DropDownList runat="server" ID="ddlmonth" class="form-control select2" 
                                        style="width:100%;">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="January">January</asp:ListItem>
								 <asp:ListItem Value="February">February</asp:ListItem>
								 <asp:ListItem Value="March">March</asp:ListItem>
								 <asp:ListItem Value="April">April</asp:ListItem>
								 <asp:ListItem Value="May">May</asp:ListItem>
								 <asp:ListItem Value="June">June</asp:ListItem>
								 <asp:ListItem Value="July">July</asp:ListItem>
								 <asp:ListItem Value="August">August</asp:ListItem>
								 <asp:ListItem Value="September">September</asp:ListItem>
								 <asp:ListItem Value="October">October</asp:ListItem>
								 <asp:ListItem Value="November">November</asp:ListItem>
								 <asp:ListItem Value="December">December</asp:ListItem>
								</asp:DropDownList>
								 </div>
								 
                               </div>
                                 <div class="col-md-4">
								 <div class="form-group">
								
									 <label>Year</label>
								 <asp:DropDownList runat="server" ID="ddlyear" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								 </div>
								 
                               </div>
											
								
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnReport" Text="Report" class="btn btn-info" 
                                          onclick="btnReport_Click" />
									 	<asp:Button runat="server" id="btnupdate" Text="DocProcess" class="btn btn-info" 
                                          onclick="btnupdate_Click" />
								 </div>
                               </div>
                               
                              <!-- end col-4 -->
                          
                             
                        <!-- end row -->
                    
                    
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->

</ContentTemplate>


</asp:UpdatePanel>




</asp:Content>

