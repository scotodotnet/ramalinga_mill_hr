﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="WeeklyOTMain.aspx.cs" Inherits="WeeklyOTMain" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>

  <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.select2').select2();
                    $('#example').dataTable();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
    </script>
     <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
         $('.select2').select2();
         $('.datepicker').datepicker({
             format: "dd/mm/yyyy",
             autoclose: true
         });
     });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
        <ContentTemplate>
           
            <!-- begin #content -->
            <div id="content" class="content">
                <!-- begin breadcrumb -->
                <ol class="breadcrumb pull-right">
                    <li><a href="javascript:;">Manual Entry</a></li>
                    <li><a href="javascript:;">Employee OT Details</a></li>
                    <li class="active">Employee OT</li>
                </ol>
                <!-- end breadcrumb -->
                <!-- begin page-header -->
                <h1 class="page-header">Employee OT</h1>
                <!-- end page-header -->

                <!-- begin row -->
                <div class="row">
                    <!-- begin col-12 -->
                    <div class="col-md-12">
                        <div>
                            <!-- begin panel -->
                            <div class="panel panel-inverse">
                                <div class="panel-heading">
                                    <div class="panel-heading-btn">
                                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    </div>
                                    <h4 class="panel-title">Employee OT</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3">
                                            <asp:LinkButton ID="lbtnAdd" runat="server" class="btn btn-success"
                                                OnClick="lbtnAdd_Click">Add New OT</asp:LinkButton>
                                        </div>
                                        <!-- begin col-2 -->
                                        <div class="col-md-2" style="visibility: hidden">
                                            <div class="form-group">
                                                <label>Adhar No</label>
                                                <asp:TextBox ID="txtAdharNo" runat="server" class="form-control" MaxLength="12"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-2 -->
                                        <!-- begin col-2 -->
                                        <div class="col-md-2" style="visibility: hidden">
                                            <div class="form-group">
                                                <label>Token No</label>
                                                <asp:TextBox ID="txtTokenNo" runat="server" class="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-2 -->
                                        <!-- begin col-2 -->
                                        <div class="col-md-2" style="visibility: hidden">
                                            <div class="form-group">
                                                <label>Location</label>
                                                <asp:TextBox ID="txtLocation" runat="server" class="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-2 -->
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                         <div class="form-group">
                                                <label>OT Date</label>
                                                <asp:TextBox ID="txtOTDate" runat="server" AutoComplete="off" class="form-control datepicker" MaxLength="12"></asp:TextBox>
                                            </div>
                                            </div>
                                         <div class="col-md-2">
                                             <br />
                                             <asp:Button ID="btnOTSearch" runat="server" Text="Search" class="btn btn-warning" OnClick="btnOTSearch_Click" />
                                         </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <table id="example" class="display table">
                                                        <thead>
                                                            <tr>
                                                                <th>Code No</th>
                                                                <th>Name</th>
                                                                <th>OT Shift</th>
                                                                <th>OT TimeIN</th>
                                                                <th>OT TimeOUT</th>
                                                                <th>OT Hr</th>
                                                                 <th>Net Amount</th>
                                                                <th>Date</th>
                                                                <th>Edit</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Eval("EmpNo")%></td>
                                                        <td><%# Eval("EmpName")%></td>
                                                         <td><%# Eval("Shift")%></td>
                                                        <td><%# Eval("TimeIn")%></td>
                                                        <td><%# Eval("TimeOut")%></td>
                                                        <td><%# Eval("NoHrs")%></td>
                                                         <td><%# Eval("netAmount")%></td>
                                                        <td><%# Eval("TransDate")%></td>
                                                        <td>
                                                            <asp:LinkButton ID="btnEditIssueEntry_Grid" class="btn btn-success btn-sm fa fa-pencil" runat="server"
                                                                Text="" OnCommand="btnEditIssueEntry_Grid_Command" CommandArgument='<%#Eval("TransDate")%>' CommandName='<%#Eval("EmpNo")%>'>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnDelete_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                Text="" OnCommand="btnDelete_Grid_Command" CommandArgument='<%#Eval("TransDate")%>' CommandName='<%#Eval("EmpNo")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this OT details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

