﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="Pay_ELCL_Mst.aspx.cs" Inherits="Pay_ELCL_Mst" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
  <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
     });
	</script>
	<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>
<!-- begin #content -->
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
	    <li class="active">EL/CL Master Details</li>
	</ol>
	<div class="col-md-8">
	<h1 class="page-header">EL/CL MASTER DETAILS</h1>
	<div class="row">
        <div class="col-md-12">
		    <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">EL/CL Master Details</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <%--<h5>STAFF AND REGULAR WORKER BONUS FIXED DETAILS</h5>--%>
                        <div class="row">
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Category</label>
								    <asp:DropDownList runat="server" ID="ddlCategory" class="form-control select2" AutoPostBack="true"
                                        onselectedindexchanged="ddlCategory_SelectedIndexChanged">
								        <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
								       <asp:ListItem Text="STAFF" Value="1"></asp:ListItem>
								       <asp:ListItem Text="LABOUR" Value="2"></asp:ListItem>
								    </asp:DropDownList>
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group">
								    <label>Employee Type</label>
								    <asp:DropDownList runat="server" ID="ddlEmployeeType" class="form-control select2" style="width:100%;" AutoPostBack="true"
                                        onselectedindexchanged="ddlEmployeeType_SelectedIndexChanged">
							 	    </asp:DropDownList>
								</div>
                            </div>
                            <div class="form-group col-md-3">
                         
								    </br>
				                    <asp:CheckBox id="chkAll" runat="server" Text="Select / UnSelect" Checked="true" Visible="true" 
                                            oncheckedchanged="chkAll_CheckedChanged" AutoPostBack="true"/>
				                    </div>
				                  
                        </div>
                        <div class="row">
                        <div class="col-md-2">
							    <div class="form-group">
								    <label>Days From</label>
								    <asp:TextBox runat="server" ID="txtDaysFrom" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                             <div class="col-md-2">
							    <div class="form-group">
								    <label>Days To</label>
								    <asp:TextBox runat="server" ID="txtDaysTo" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                           
                            <div class="col-md-2">
							    <div class="form-group">
								    <label>EL Days</label>
								    <asp:TextBox runat="server" ID="txtELDays" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                             <div class="col-md-2">
							    <div class="form-group">
								    <label>CL Value</label>
								    <asp:TextBox runat="server" ID="txtCLValue" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                         
                        </div>
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-4">
								<div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                        onclick="btnSave_Click" />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                        onclick="btnClear_Click"  />
						    	</div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                       <div class="row">
                                        <!-- table start -->
                                        <div class="col-md-8">
                                            <div class="row">
                                                <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                    <HeaderTemplate>
                                                        <table id="example" class="display table">
                                                            <thead>
                                                                <tr>
                                                                    <th>S.No</th>
                                                                    
                                                                    <th>EmpType</th>
                                                                    <th>DeptName</th>
                                                                   <th>Days FRom</th>
                                                                   <th>Days To</th>
                                                                    <th>EL Days</th>
                                                                   <%--<th>CL Days</th>--%>
                                                                    <th>Mode</th>
                                                                </tr>
                                                            </thead>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Container.ItemIndex + 1 %></td>
                                                            <td><%# Eval("EmpType")%></td>
                                                            <td><%# Eval("DeptName")%></td>
                                                            <td><%# Eval("DaysFrom")%></td>
                                                            <td><%# Eval("DaysTo")%></td>
                                                           <td><%# Eval("ELDays")%></td>
                                                            <%--<td><%# Eval("CLDays")%></td>--%>
                                                            <td>
                                                               
                                                                <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                    Text="" OnCommand="btnDeleteEnquiry_Grid_Command" CommandArgument='<%# Eval("DeptCode")%>' CommandName='<%# Eval("Category")+","+Eval("EmployeeType")%>'
                                                                    CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this EL/CL details?');">
                                                                </asp:LinkButton>
                                                            </td>

                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate></table></FooterTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                        <!-- table End -->
                                    </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
  <div class="col-md-4">
  <h1 class="page-header"></h1>
  <div class="row">
  <div class="col-md-12">
		    <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Department Details</h4>
                </div>
                </div>
       
       
       <div class="col-md-12">
				        <div class="row">
				            <asp:Panel ID="GVPanel" runat="server" ScrollBars="None" Visible="true">
				                <asp:GridView id="GVModule" runat="server" AutoGenerateColumns="false" 
				                ClientIDMode="Static" class="gvv display table">
				                    <Columns>
				                        <asp:TemplateField  HeaderText="DeptCode" Visible="false">
				                            <ItemTemplate>
				                                <asp:Label id="DeptCode" runat="server" Text='<%# Eval("DeptCode") %>'/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:BoundField DataField="DeptName" HeaderText="Dept Name" />
				                        <asp:TemplateField  HeaderText="Add">
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkSelect" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                     
				                    </Columns>
				                </asp:GridView>
				            </asp:Panel>
					    </div>
					</div>
                </div>
  </div>
  </div>
</div>



</asp:Content>

