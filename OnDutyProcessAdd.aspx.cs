﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class OnDutyProcess : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: OnDuty Process";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            load_TokenNo();
            if (Session["TransID"] != null)
            {
                txtTransid.Text = Session["TransID"].ToString();
                txtTransid.Enabled = false;
                btnSearch_Click(sender, e);
            }
        }
    }
    public void load_TokenNo()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlTokenNo.Items.Clear();
        query = "Select  ExistingCode from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'  And IsActive='Yes' ";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlTokenNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ExistingCode"] = "-Select-";
        dr["ExistingCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlTokenNo.DataTextField = "ExistingCode";
        ddlTokenNo.DataValueField = "ExistingCode";
        ddlTokenNo.DataBind();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        DataTable DT = new DataTable();

        SSQL = "Select * from OnDuty_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " and TransID='" + txtTransid.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            txtTransid.Text = DT.Rows[0]["TransID"].ToString();
            ddlTokenNo.SelectedValue = DT.Rows[0]["TokenNo"].ToString();
            txtEmpName.Text = DT.Rows[0]["EmpName"].ToString();
            txtFromDate.Text = DT.Rows[0]["ONDutyFromDate"].ToString();
            txtToDate.Text  = DT.Rows[0]["ONDutyToDate"].ToString();
            txtDays.Text = DT.Rows[0]["Days"].ToString();
            txtFromArea.Text = DT.Rows[0]["FromArea"].ToString();
            txtToArea.Text = DT.Rows[0]["ToArea"].ToString();
            txtReason.Text = DT.Rows[0]["Reason"].ToString();
            txtVechileno.Text = DT.Rows[0]["VechileNo"].ToString();
            txtCharge.Text= DT.Rows[0]["TravelCharge"].ToString();
            txtReturnDate.Text = DT.Rows[0]["ONDutyReturnDate"].ToString();
            btnSave.Text = "Update";
        }

    }
    protected void ddlTokenNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();

        query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();

        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Session.Remove("TransID");
        Response.Redirect("OnDutyProcessMain.aspx");
    }
    private void Clear_All_Field()
    {
        txtTransid.Text = "";
        txtEmpName.Text = "";
        ddlTokenNo.SelectedValue = "-Select-";
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtDays.Text = "";
        
        txtFromArea.Text = ""; txtToArea.Text = "";
        txtReason.Text = "";
        txtVechileno.Text = "";
        txtCharge.Text = "";
        txtReturnDate.Text = "";

        Session.Remove("TransID");
        
        btnSave.Text = "Save";


    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataTable DT = new DataTable();
        if (btnSave.Text == "Save")
        {
            SSQL = "Select * from OnDuty_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " and TransID='" + txtTransid.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count != 0)
            {
                //Delete OLD Record
                SSQL = "Delete from OnDuty_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " and TransID='" + txtTransid.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }


            //Insert Commision Voucher
            SSQL = "Insert Into OnDuty_Mst(CompCode,LocCode,TransID,TokenNo,EmpName,ONDutyFromDate,ONDutyToDate,Days,FromArea,ToArea,Reason,VechileNo,TravelCharge,ONDutyReturnDate)";
            SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtTransid.Text + "','" + ddlTokenNo.SelectedItem.Text + "','" + txtEmpName.Text + "',";
            SSQL = SSQL + "'" + txtFromDate.Text + "','" + txtToDate.Text + "','" + txtDays.Text + "','" + txtFromArea.Text + "','" + txtToArea.Text + "',";
            SSQL = SSQL + "'" + txtReason.Text + "','" + txtVechileno.Text + "','" + txtCharge.Text + "','" + txtReturnDate.Text + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);



            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('ON Duty Deatils Saved Successfully..');", true);
            Clear_All_Field();
        }
        else if (btnSave.Text == "Update")
        {
            SSQL = "Select * from OnDuty_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " and TransID='" + txtTransid.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            string returndate = DT.Rows[0]["ONDutyReturnDate"].ToString();
            if (returndate == "")
            {

                SSQL = " Update OnDuty_Mst Set TokenNo='" + ddlTokenNo.SelectedItem.Text + "',EmpName='" + txtEmpName.Text + "',ONDutyFromDate='" + txtFromDate.Text + "',ONDutyToDate='" + txtToDate.Text + "', ";
                SSQL = SSQL + " Days='" + txtDays.Text + "',FromArea='" + txtFromArea.Text + "',ToArea='" + txtToArea.Text + "',Reason='" + txtReason.Text + "', ";
                SSQL = SSQL + " VechileNo='" + txtVechileno.Text + "',TravelCharge='" + txtCharge.Text + "',ONDutyReturnDate='" + txtReturnDate.Text + "'";
                SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'and TransID='" + txtTransid.Text + "' ";
                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('ONDuty Details Updated Successfully..');", true);
                Clear_All_Field();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Details Already Completed..');", true);
            }

        }
    }
}
