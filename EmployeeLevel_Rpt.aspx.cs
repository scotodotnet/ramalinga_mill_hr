﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class EmployeeLevel_Rpt : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string NetBase;
    string NetFDA;
    string NetVDA;
    string NetHRA;
    string Nettotal;
    string NetPFEarnings;
    string NetPF;
    string NetESI;
    string NetUnion;
    string NetAdvance;
    string NetAll1;
    string NetAll2;
    string NetAll3;
    string NetAll4;
    string NetAll5;
    string NetDed1;
    string NetDed2;
    string NetDed3;
    string NetDed4;
    string NetDed5;
    string HomeDays;
    string Halfnight;
    string FullNight;
    string DayIncentive;
    string Spinning;
    string ThreeSided;
    string NetLOP;
    string NetStamp;
    string NetTotalDeduction;
    string NetOT;
    string NetAmt;
    string Network;
    string totNFh;
    string totweekoff;
    string totCL;
    string totwork;
    string Roundoff;
    DateTime MyDate;
    DateTime MyDate1;
    DateTime MyDate2;
    static decimal AdvAmt;
    static string ID;
    static string Adv_id = "";
    static string Dec_mont = "0";
    static string Adv_BalanceAmt = "0";
    static string Adv_due = "0";
    static string Increment_mont = "0";
    static decimal val;
    static string EmployeeDays = "0";
    string MyMonth;
    static string cl = "0";
    string TempDate;
    static string Fixedsal = "0";
    static string FixedOT = "0";
    static string Tot_OThr = "0";
    static string Emp_ESI_Code = "";
    static string NetPay_Grand_Total = "0";
    static string NetPay_Grand_Total_Words = "";
    static bool isUK = false;
    string SessionPayroll;
    string SSQL = "";

    string SessionUserName;
    string SessionUserID;
    string SessionRights;

    //  string Wages = "";

    string SessionCompanyName;
    string SessionLocationName;


    System.Web.UI.WebControls.DataGrid grid =
                          new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionCompanyName = Session["CompanyName"].ToString();
        SessionLocationName = Session["LocationName"].ToString();
        SessionPayroll = Session["SessionEpay"].ToString();

        Stafflabour = Request.QueryString["Wages"].ToString();


        Report();

    }

    private void Report()
    {
        SSQL = "";
        SSQL = "Select  ROW_NUMBER() OVER (ORDER BY ED.DeptCode) as [S.No],('&nbsp;'+ED.ExistingCode) as [CODE],ED.FirstName as NAME,ED.DeptName as [DEPARTMENT],";
        SSQL = SSQL + "SK.Skill1 as [1],SK.Skill2 as [2],SK.Skill3 as [3] from Employee_Mst ED inner join MstEmpLevel SK on SK.MachineID=ED.MachineID ";
        SSQL = SSQL + " where ED.CompCode='" + SessionCcode + "' and ED.LocCode='" + SessionLcode + "' and SK.Ccode='" + SessionCcode + "' and SK.Lcode='" + SessionLcode + "'";

        if (Stafflabour != "-Select-")
        {
            SSQL = SSQL + " and ED.Wages='" + Stafflabour + "'";
        }
        SSQL = SSQL + " order by CAST(ED.DeptCode as int),ED.CatName Asc";

        DataTable Dt = new DataTable();
        Dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Dt != null && Dt.Rows.Count > 0)
        {
            grid.DataSource = Dt;
            grid.DataBind();

           

            string attachment = "attachment;filename=Bonus_Recociliation.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);

            Response.Write("<table>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan=" + Dt.Columns.Count + " style='font-size:12.0pt;font-weight:bold;text-decoration:underline;''>");
            Response.Write(SessionCompanyName + " - " + SessionLocationName);
            Response.Write("</td>");
            Response.Write("</tr>");
            //Response.Write("<tr align='Center'>");
            //Response.Write("<td colspan=" + Dt.Columns.Count + " style='font-size:10.0pt;font-weight:bold;text-decoration:underline;''>");
            //Response.Write("</td>");
            //Response.Write("</tr>");
            Response.Write("</table>");

            //Heading Fixed
            Response.Write("<table>");
            Response.Write("<tr>");
            Response.Write("<td align='Center' colspan=" + Dt.Columns.Count + " style='font-size:10.0pt;font-weight:bold;text-decoration:underline;''>");
            Response.Write("EMPLOYEE LEVEL DETAILS");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");

            //Grid Write Excel
            Response.Write(stw.ToString());

            Response.End();
            Response.Clear();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Data Found')", true);
        }
    }
}