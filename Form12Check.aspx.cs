﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Globalization;

public partial class Form12Check : System.Web.UI.Page
{
    ReportDocument rd = new ReportDocument();
    DataTable dtdlocation = new DataTable();
    string fromdate;
    string todate;
    string Wages;
    string DeptName;
    string CatName;
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionEpay;
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable();
    string SessionUserType;
    string SSQL = "";
    string mIpAddress_IN;
    string mIpAddress_OUT;
    DataTable dsEmployee = new DataTable();
    DateTime date1 = new DateTime();
    DateTime date2 = new DateTime();
    string RegNo;

    string Address1;
    string Address2;
    string city;
    string Pincode;
    string PF_DOJ_Get_str;
    DateTime PF_DOJ_Get;
    string Year = "";
    
    System.Web.UI.WebControls.DataGrid grid =
                                    new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        SessionCompanyName = Session["CompanyName"].ToString();
        SessionLocationName= Session["LocationName"].ToString();

        Wages = Request.QueryString["Wages"].ToString();
        DeptName= Request.QueryString["DeptName"].ToString();
        CatName= Request.QueryString["CatName"].ToString();
        Year= Request.QueryString["Year"].ToString();

        excel();
    }
    public void excel()
    {

        // add Company name and Location 
        DataTable dt_Cat = new DataTable();

        SSQL = "";
        SSQL = "Select  ROW_NUMBER() OVER(ORDER BY ED.ExistingCode) as [S.NO],ED.ExistingCode as Code,ED.Address2 as Address,ED.FirstName,ED.Designation,";
        SSQL = SSQL + " FD.Names ,Convert(varchar,ED.BirthDate,104) as DOB,ED.ShiftName from Employee_Mst ED ";
        SSQL = SSQL + " left join FamilyDetails FD on  FD.ExistingCode=ED.Existingcode and FD.FamilyTypeCode = CASE WHEN ED.Gender='FEMALE' and ED.MaritalStatus='Married' then '5' else '1' end ";
        SSQL = SSQL + " where ED.CompCode='" + SessionCcode + "' and ED.LocCOde='" + SessionLcode + "' and ED.IsActive='Yes'";
        if (Wages != "" &&Wages!="-Select-")
        {
            SSQL = SSQL + " and ED.Wages='" + Wages + "'";
        }
        if (CatName != "-Select-")
        {
            SSQL = SSQL + " and ED.CatName='" + CatName + "'";
        }
        if (DeptName != "" && DeptName!="-Select-")
        {
            SSQL = SSQL + " and ED.DeptName='" + DeptName + "'";
        }
        SSQL = SSQL + " order by ED.ExistingCode";
        DataCells = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "";
        SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionEpay + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        //dt = objdata.RptEmployeeMultipleDetails(SSQL);
        dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
        string CmpName = "";
        string Cmpaddress = "";
        if (dt_Cat.Rows.Count > 0)
        {
            CmpName = dt_Cat.Rows[0]["Cname"].ToString();
            Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
        }
    
        SSQL = "";
        SSQL = "Select Register_No from Location_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            RegNo = dt.Rows[0]["Register_No"].ToString();
        }
        if (DataCells.Rows.Count > 0)
        {
            rd.Load(Server.MapPath("Payslip/FORM12.rpt"));
            rd.SetDataSource(DataCells);
            if (Wages != "CONTRACTCLEANING I" && Wages != "CONTRACTCLEANING II")
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
                rd.DataDefinition.FormulaFields["Location"].Text = "'" + Cmpaddress + "'";
            }
            else
            {
                rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + "" + "'";
                rd.DataDefinition.FormulaFields["Location"].Text = "'" + "" + "'";
            }

            rd.DataDefinition.FormulaFields["RegNo"].Text = "'" + RegNo + "'";
            if (Year == "0"|| Year == "")
            {
                Year = DateTime.Now.ToString("yyyy");
            }
            rd.DataDefinition.FormulaFields["Year"].Text = "'" + Year + "'";
            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = rd;
           // CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }

     //   string attachment = "attachment;filename=Form12.xls";
     //   Response.ClearContent();
     //   Response.AddHeader("content-disposition", attachment);
     //   Response.ContentType = "application/ms-excel";
     ////   grid.HeaderStyle.Font.Bold = true;
     //   grid.ShowHeader = false;
     //   System.IO.StringWriter stw = new System.IO.StringWriter();
     //   HtmlTextWriter htextw = new HtmlTextWriter(stw);
     //   grid.RenderControl(htextw);

     //   Response.Write("<table>");
     //   Response.Write("<tr>");
     //   Response.Write("<td colspan='17' align='Center'>");
     //   Response.Write("<a style=\"font-weight:bold\">FORM - 12</a>");
     //   Response.Write("</td>");
     //   Response.Write("</tr>");
     //   Response.Write("<tr>");
     //   Response.Write("<td colspan='17' align ='center'>");
     //   Response.Write("<a style=\"font-weight:bold\">( PRESCRIBED UNDER RULE 80 )</a>");
     //   Response.Write("</td>");

     //   Response.Write("<td colspan='17' align ='Left'>");
     //   Response.Write("<a style=\"font-weight:bold\">Factory Regn.No" + RegNo + "</a>");
     //   Response.Write("</td>");

     //   Response.Write("</tr>");

     //   Response.Write("<tr Font-Bold='true' align='Left'>");
     //   Response.Write("<td colspan='17'>");
     //   Response.Write("<a style=\"font-weight:bold\">" + CmpName + "</a>");
     //   Response.Write("</td>");
     //   Response.Write("<td colspan='17' align ='Left'>");
     //   Response.Write("<a style=\"font-weight:bold\">Year</a>");
     //   Response.Write("</td>");
     //   Response.Write("</tr>");
     //   Response.Write("<tr Font-Bold='true' align='left'>");

     //   Response.Write("<td colspan='17'>");
     //   Response.Write("<a style=\"font-weight:bold\">" + Cmpaddress + "</a>");
     //   Response.Write("</td>");
     //   Response.Write("<td>");
     //   Response.Write("<a style=\"font-weight:bold\">PAGE NO</a>");
     //   Response.Write("</td>");
     //   Response.Write("</tr>");
     //   Response.Write("<tr align='center'>");

     //   Response.Write("<td>");
     //   Response.Write("<a style=\"font-weight:bold\" align='center'>S.NO </br></a>");
     //   Response.Write("</td>");
     //   // Response.Write("</td>");
     //   //  Response.Write("<td>");
     //   Response.Write("<td>");
     //   Response.Write("<a style=\"font-weight:bold\" align='center'>CODE</a>");
     //   Response.Write("</td>");
     //   Response.Write("<td>");
     //   Response.Write("</td>");

     //   Response.Write("<td>");
     //   Response.Write("<a style=\"font-weight:bold\">EMPLOYEE NAME ADDRESS</a>");
     //   Response.Write("</td>");
     //   Response.Write("</td>");
     //   Response.Write("<td>");
     //   Response.Write("<td>");
     //   Response.Write("<a style=\"font-weight:bold\">FATHERS/HUSBAND's NAME</a>");
     //   Response.Write("</td>");
     //   Response.Write("</td>");
     //   Response.Write("<td>");
     //   Response.Write("<td>");
     //   Response.Write("<a style=\"font-weight:bold\">DESIGNATION</a>");
     //   Response.Write("</td>");
     //   Response.Write("</td>");
     //   Response.Write("<td>");
     //   Response.Write("<td>");
     //   Response.Write("<a style=\"font-weight:bold\" style=\'word-wrap: break-word\'>DOB</a>");
     //   Response.Write("</td>");
     //   Response.Write("</td>");
     //   Response.Write("<td>");
     //   Response.Write("<td>");
     //   Response.Write("<a style=\"font-weight:bold\">GRP.LR.</a>");
     //   Response.Write("</td>");
     //   Response.Write("</td>");
     //   Response.Write("<td>");
     //   Response.Write("<td>");
     //   Response.Write("<a style=\"font-weight:bold\">RELAY SHIFT NO.</a>");
     //   Response.Write("</td>");
     //   Response.Write("</td>");
     //   Response.Write("<td>");
     //   Response.Write("<td>");
     //   Response.Write("<a style=\"font-weight:bold\">CERT.TKN.NO</a>");
     //   Response.Write("</td>");
     //   Response.Write("</td>");
     //   Response.Write("<td>");

     //   Response.Write("<td>");
     //   Response.Write("<a style=\"font-weight:bold\">REMARKS</a>");
     //   Response.Write("</td>");

     //   Response.Write("</tr>");
     //   Response.Write("</table>");
     //   Response.Write(stw.ToString());
     //   Response.End();
     //   Response.Clear();
    }

}

