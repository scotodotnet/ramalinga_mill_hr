﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="MstTourAllowance.aspx.cs" Inherits="MstTourAllowance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="assets/js/master_list_jquery.min.js"></script>
    <script src="assets/js/master_list_jquery-ui.min.js"></script>
    <link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').dataTable();
        });
    </script>
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('.select2').select2();
                }
            });
        };
    </script>
    <!-- begin #content -->
    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li class="active">Tour Allowance Master Details</li>
        </ol>
        <h1 class="page-header">Tour Allowance Master</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">Tour Allowance Master</h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <%--<h5>STAFF AND REGULAR WORKER BONUS FIXED DETAILS</h5>--%>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Category</label>
                                        <asp:DropDownList runat="server" ID="ddlCategory" class="form-control select2" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                                            <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="STAFF" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="LABOUR" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Employee Type</label>
                                        <asp:DropDownList runat="server" ID="ddlEmployeeType" class="form-control select2" Style="width: 100%;" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Department</label>
                                        <asp:DropDownList runat="server" ID="ddlDeptName" class="form-control select2" Style="width: 100%;">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Minimum Wages</label>
                                        <asp:TextBox runat="server" ID="txtMinimumWages" Text="0.00" class="form-control" Style="width: 100%;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Maximum Wages</label>
                                        <asp:TextBox runat="server" ID="txtMaximumWages" Text="0.00" class="form-control" Style="width: 100%;"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Allowance/Day</label>
                                        <asp:TextBox runat="server" ID="txtAllowance" Text="0.00" class="form-control" Style="width: 100%;"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Deduction Amt</label>
                                        <asp:TextBox runat="server" ID="txtDeduction" Text="0.00" class="form-control" Style="width: 100%;"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5"></div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <br />
                                        <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success" OnClick="btnSave_Click" />
                                        <asp:Button runat="server" ID="btnClr" Text="Clear" class="btn btn-danger" OnClick="btnClr_Click" />
                                    </div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            <div class="row">
                                <!-- table start -->
                                <div class="col-md-12">
                                    <div class="row">
                                        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                            <HeaderTemplate>
                                                <table id="example" class="display table">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Category</th>
                                                            <th>EmpType</th>
                                                            <th>Department</th>
                                                            <th>Minimum Wages</th>
                                                            <th>Maximum Wages</th>
                                                            <th>Allowance / Day</th>
                                                            <th>Deduction Amt</th>
                                                            <th>Mode</th>
                                                        </tr>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                    <td><%# Eval("CateName")%></td>
                                                    <td><%# Eval("EmpTypeName")%></td>
                                                    <td><%# Eval("DeptName")%></td>
                                                    <td><%# Eval("MinWages")%></td>
                                                    <td><%# Eval("MaxWages")%></td>
                                                    <td><%# Eval("Allowance")%></td>
                                                    <td><%# Eval("Deduction")%></td>
                                                    <td>
                                                        <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil" runat="server"
                                                            Text="" OnCommand="btnEditEnquiry_Grid_Command" CommandArgument='<%# Eval("MinWages")+","+Eval("MaxWages")%>' CommandName='<%# Eval("CateCode")+","+Eval("EmpTypeCode")%>'>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                            Text="" OnCommand="btnDeleteEnquiry_Grid_Command" CommandArgument='<%# Eval("MinWages")+","+Eval("MaxWages")%>' CommandName='<%# Eval("CateCode")+","+Eval("EmpTypeCode")%>'
                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Bonus details?');">
                                                        </asp:LinkButton>
                                                    </td>

                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate></table></FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <!-- table End -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
