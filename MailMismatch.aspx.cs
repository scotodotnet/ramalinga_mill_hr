﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using System.Net.Mail;

public partial class MailMismatch : System.Web.UI.Page
{
    DataTable DataCellsImpro = new DataTable();

    DataTable DataCellsMiss = new DataTable();

    string AttachfileName_Invoice_Bill = "";

    string AttachfileName_Improper = "";

    string AttachfileName_Miss = "";


    DataTable Logindays = new DataTable();

    string WitSalesOrdNo = "";
    string status = "";
    MailMessage mail = new MailMessage();
    string ShiftType1 = "";
    string Date = "";
    string Status = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable DataCellNew = new DataTable();
    DataTable dt = new DataTable();


    string Datestr = "";
    string Datestr1 = "";
    string ShiftType = "";
    DataTable mDataSet = new DataTable();

    DataTable mLocalDS_INTAB = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    string Date_Value_Str;
    string Time_IN_Str = "";
    string Time_Out_Str = "";
    int time_Check_dbl = 0;
    string Total_Time_get = "";

    DataRow dtRow;

    DataTable MLocal_Day = new DataTable();
    DataTable Payroll_DS = new DataTable();

    string Shift_Start_Time;
    string Shift_End_Time;
    string Employee_Time = "";
    DateTime ShiftdateStartIN = new DateTime();
    DateTime ShiftdateEndIN = new DateTime();
    DateTime EmpdateIN = new DateTime();
    string ColumnName = "";
    double Totol_Hours_Check = 0;
    double Totol_Hours_Check_1 = 8.2;
    string[] Time_Split_Str;
    string Time_IN_Get;
    string Time_Out_Update = "";
    long Random_No_Get;
    string[] Time_Minus_Value_Check;

    DateTime Date1;
    string todate;
    string fromdate;


    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = "THEN";
        SessionLcode = "UNIT I";
        SessionAdmin = "1";
        SessionCompanyName = "Thenpandi Spinning Mill";
        SessionLocationName = "UNIT I";
        SessionUserType = "1";
        Date1 = DateTime.Now.AddDays(-1);
        DateTime Date2 = DateTime.Now.AddDays(-1);

        DateTime dateGiven = DateTime.Now;
        dateGiven = dateGiven.AddMonths(-1);
        DateTime thisMonth = new DateTime(dateGiven.Year, dateGiven.Month, 1);
        //thisMonth.ToString("yyyy-MM-01");
        //fromdate = "01-" + dateGiven.Month + "-" + dateGiven.Year;
        //todate = thisMonth.AddMonths(1).AddDays(-1).Day + "-" + dateGiven.Month + "-" + dateGiven.Year;

        //fromdate = "02/10/2018";

        fromdate = Convert.ToString(Date1.ToString("dd/MM/yyyy"));
        todate = Convert.ToString(Date2.ToString("dd/MM/yyyy"));

        //GetAttdDayWise_Change();
        GetAttdDayWise_MisMatch_Change();

        //GetAttdDayWise_Change();
        //GetAttdDayWise_Improper_Change();




        InvoiceMail_AttachandSend("", "tpsmhrm@gmail.com", "padmanabhanscoto@gmail.com", "Daily Reports");

        ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
        //if (Session["Isadmin"] == null)
        //{
        //    Response.Redirect("Default.aspx");
        //    Response.Write("Your session expired");
        //}
        //else
        //{
        //    if (!IsPostBack)
        //    {

        //        Page.Title = "Spay Module | Report-Day Attendance Day Wise";
        //        //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
        //        //li.Attributes.Add("class", "droplink active open");
        //    }
        //    SessionCcode = Session["Ccode"].ToString();
        //    SessionLcode = Session["Lcode"].ToString();
        //    SessionAdmin = Session["Isadmin"].ToString();
        //    //SessionCompanyName = Session["CompanyName"].ToString();
        //    //SessionLocationName = Session["LocationName"].ToString();
        //    SessionUserType = Session["Isadmin"].ToString();

        //    Status = Request.QueryString["Status"].ToString();
        //    ShiftType1 = Request.QueryString["ShiftType1"].ToString();
        //    Date = Request.QueryString["Date"].ToString();
        //    Division = Request.QueryString["Division"].ToString();
        //    //if (SessionUserType == "1")
        //    //{
        //    //    GetAttdDayWise_Change();
        //    //}
        //    if (SessionUserType == "2")
        //    {
        //        NonAdminGetAttdDayWise_Change();
        //    }
        //    else
        //    {
        //        GetAttdDayWise_Change();
        //    }
        //}
    }
    public void GetAttdDayWise_Change()
    {
        string TableName = "";

        //if (Status == "Pending")
        //{
        //    TableName = "Employee_Mst_New_Emp";
        //}

        //else
        //{

        //}

        TableName = "Employee_Mst";

        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("Dept");
        DataCell.Columns.Add("Type");
        DataCell.Columns.Add("Shift");


        DataCell.Columns.Add("EmpCode");
        DataCell.Columns.Add("ExCode");
        DataCell.Columns.Add("Name");
        DataCell.Columns.Add("TimeIN");
        DataCell.Columns.Add("TimeOUT");
        DataCell.Columns.Add("MachineID");
        DataCell.Columns.Add("Category");
        DataCell.Columns.Add("SubCategory");
        DataCell.Columns.Add("TotalMIN");
        DataCell.Columns.Add("GrandTOT");
        DataCell.Columns.Add("ShiftDate");
        DataCell.Columns.Add("CompanyName");
        DataCell.Columns.Add("LocationName");

        SSQL = "";
        SSQL = "select LD.MachineID,LD.ExistingCode,isnull(LD.DeptName,'') As DeptName,LD.Shift,LD.TypeName,isnull(LD.FirstName,'') as FirstName,";
        SSQL = SSQL + " LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName from LogTime_Days LD";
        SSQL = SSQL + " inner join " + TableName + " EM on EM.MachineID = LD.MachineID";
        //SSQL = SSQL + " inner join MstCategory MC on EM.WageCategoty = MC.CateID";
        //SSQL = SSQL + " inner join AgentMst AM on EM.BrokerName = AM.AgentID";
        //SSQL = SSQL + " inner join MstGrade MG on EM.Grade = MG.GradeID";
        SSQL = SSQL + " where LD.CompCode='" + SessionCcode + "' ANd LD.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' ANd EM.LocCode='" + SessionLcode + "'";
        //SSQL = SSQL + " And AM.CompCode='" + Session["SessionCcode"].ToString() + "' ANd AM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
        //SSQL = SSQL + " And MG.CompCode='" + Session["SessionCcode"].ToString() + "' ANd MG.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";

        //if (Division != "-Select-")
        //{
        //    SSQL = SSQL + " And EM.Division = '" + Division + "'";
        //}
        //if (ShiftType1 != "ALL")
        //{
        //    if (ShiftType1 == "GENERAL")
        //    {
        //        SSQL = SSQL + " And (LD.Wages='MANAGER' or LD.Wages='STAFF' or LD.Wages='Watch & Ward')";
        //    }
        //    else
        //    {
        //        SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
        //    }
        //}
        if (fromdate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(fromdate).ToString("dd/MM/yyyy") + "',103)";
        }
        //if (ShiftType1 == "GENERAL")
        //{
        //    SSQL = SSQL + "  And TimeIN!=''  ";
        //}
        //else
        //{

        //}
        // 
        SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";
        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);


        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        if (AutoDTable.Rows.Count != 0)
        {
            int sno = 1;
            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                DataCell.NewRow();
                DataCell.Rows.Add();


                DataCell.Rows[i]["SNo"] = sno;
                DataCell.Rows[i]["Dept"] = AutoDTable.Rows[i]["DeptName"].ToString();
                DataCell.Rows[i]["Type"] = AutoDTable.Rows[i]["TypeName"].ToString();
                if (ShiftType1 == "GENERAL")
                {
                    DataCell.Rows[i]["Shift"] = AutoDTable.Rows[i]["Shift"].ToString();
                    //DataCell.Rows[i]["Shift"] = "GENERAL";//AutoDTable.Rows[i]["Shift"].ToString();
                }
                else
                {
                    DataCell.Rows[i]["Shift"] = AutoDTable.Rows[i]["Shift"].ToString();
                }
                DataCell.Rows[i]["EmpCode"] = AutoDTable.Rows[i]["MachineID"].ToString();
                DataCell.Rows[i]["ExCode"] = AutoDTable.Rows[i]["ExistingCode"].ToString();
                DataCell.Rows[i]["Name"] = AutoDTable.Rows[i]["FirstName"].ToString();
                DataCell.Rows[i]["TimeIN"] = AutoDTable.Rows[i]["TimeIN"].ToString();
                DataCell.Rows[i]["TimeOUT"] = AutoDTable.Rows[i]["TimeOUT"].ToString();
                DataCell.Rows[i]["MachineID"] = AutoDTable.Rows[i]["MachineID"].ToString();
                DataCell.Rows[i]["Category"] = AutoDTable.Rows[i]["CatName"].ToString();
                DataCell.Rows[i]["SubCategory"] = AutoDTable.Rows[i]["SubCatName"].ToString();
                DataCell.Rows[i]["TotalMIN"] = AutoDTable.Rows[i]["Total_Hrs"].ToString();
                DataCell.Rows[i]["GrandTOT"] = AutoDTable.Rows[i]["Total_Hrs"].ToString();
                DataCell.Rows[i]["ShiftDate"] = Date;
                DataCell.Rows[i]["CompanyName"] = name.ToString();
                DataCell.Rows[i]["LocationName"] = SessionLcode;


                sno += 1;
            }


            string Customer_Mail_ID = "tpsmhrm@gmail.com";
            string Agent_Mail_ID = "padmanabhanscoto@gmail.com";


            string[] Check_Val = fromdate.Split(new[] { "/", "_" }, StringSplitOptions.RemoveEmptyEntries);

            ds.Tables.Add(DataCell);
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/AttendanceMail.rpt"));
            report.DataDefinition.FormulaFields["ShiftDate"].Text = "'" + fromdate + "'";
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            //CrystalReportViewer1.ReportSource = report;

            //string LastDate = fromdate.Split("/", "_");

            string Server_Path = Server.MapPath("~");

            string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
            AttachfileName_Invoice_Bill = Server_Path + "/Daily_Report/DailyAttendance_" + Check_Val[0] + Check_Val[1] + Check_Val[2] + ".pdf";

            if (File.Exists(AttachfileName_Invoice_Bill))
            {
                File.Delete(AttachfileName_Invoice_Bill);
            }

            report.ExportToDisk(ExportFormatType.PortableDocFormat, AttachfileName_Invoice_Bill);

            //GetAttdDayWise_Improper_Change();

            //InvoiceMail_AttachandSend(WitSalesOrdNo, Customer_Mail_ID, Agent_Mail_ID, "");
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Mail Send Successfully..');", true);
            //lblUploadSuccessfully.Text = "Mail Send Successfully..";






            //Mail Start
            //mail.To.Add("tpsmhrm@gmail.com");
            //mail.Bcc.Add("padmanabhanscoto@gmail.com");

            //mail.From = new MailAddress("aatm2005@gmail.com");
            //mail.Subject = "Thepandi Spinning Mill - UNIT I";
            //mail.Body = "Find the attachment for Daily Report";
            //mail.IsBodyHtml = true;

            ////mail.Attachments.Add(new Attachment(ms, "DayWise1.rpt", "crystal/DayWise1.rpt"));
            //mail.Attachments.Add(new Attachment(report.ExportToStream(ExportFormatType.PortableDocFormat), "Daily Report.pdf"));

            ////Daywise_Mail();
            ////Daywise_Mail_Des();

            //SmtpClient smtp = new SmtpClient();
            //smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address
            //smtp.Credentials = new System.Net.NetworkCredential("aatm2005@gmail.com", "Altius@2005");  //Or your Smtp Email ID and Password
            //smtp.EnableSsl = true;
            //smtp.Send(mail);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }
    public void InvoiceMail_AttachandSend(string Invoice_Bill_No, string Customer_Mail_Str, string Agent_Mail_ID_Str, string Mail_Subject)
    {
        string query = "";
        string To_Address = "";
        string CC_Mail_Address = "";
        string BCC_Mail_Address = "";
        DataTable Order_Mail_DT = new DataTable();
        DataTable DT_T = new DataTable();
        DataTable Party_DT = new DataTable();
        if (Customer_Mail_Str != "")
        {

            To_Address = Customer_Mail_Str;
            if (Agent_Mail_ID_Str != "") { To_Address = To_Address + "," + Agent_Mail_ID_Str; }

            //CC Address Add
            CC_Mail_Address = "aatm2005@gmail.com";// "kalyan.r@scoto.in";

            MailMessage mm = new MailMessage("THENPANDI SPINNING<aatm2005@gmail.com>", To_Address);
            if (Mail_Subject != "")
            {
                mm.Subject = Mail_Subject;
            }
            else
            {
                mm.Subject = "Daily Report";
            }
            mm.Body = "Find the attachment for Daily Details";
            //mm.Attachments.Add(new Attachment(AttachfileName_Invoice_Bill));
            //mm.Attachments.Add(new Attachment(AttachfileName_Improper));
            mm.Attachments.Add(new Attachment(AttachfileName_Miss));


            if (CC_Mail_Address != "") { mm.CC.Add(CC_Mail_Address); }
            //if (BCC_Mail_Address != "") { mm.Bcc.Add(BCC_Mail_Address); }

            mm.IsBodyHtml = false;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";   // We use gmail as our smtp client
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = new System.Net.NetworkCredential("aatm2005@gmail.com", "Altius@2005");

            smtp.Send(mm);

            mm.Attachments.Dispose();
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Mail Sent Successfully..');", true);

        }


    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = "0";
        BALDataAccess objdata_new = new BALDataAccess();

        string query = "Select * from Employee_Mst where MachineID_Encrypt='" + encryptpwd + "'";
        DataTable DT = new DataTable();
        DT = objdata_new.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            decryptpwd = DT.Rows[0]["MachineID"].ToString();
        }
        else
        {
            query = "Select * from Employee_Mst_New_Emp where MachineID_Encrypt='" + encryptpwd + "'";
            DT = objdata_new.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                decryptpwd = DT.Rows[0]["MachineID"].ToString();
            }
            else
            {
                decryptpwd = "0";
            }
        }

        //UTF8Encoding encodepwd = new UTF8Encoding();
        //Decoder Decode = encodepwd.GetDecoder();
        //byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        //int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        //char[] decoded_char = new char[charCount];
        //Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        //decryptpwd = new String(decoded_char);
        return decryptpwd;
    }
    public static string GetRandom(int Min, int Max)
    {
        System.Random Generator = new System.Random();

        return Generator.Next(Min, Max).ToString();


    }
    public static string UTF8Encryption_OLD(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }

    public void GetImproperPunch()
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable dt3 = new DataTable();
        double Count = 0;
        double Count1;

        //DateTime dayy = Convert.ToDateTime(Date);



        string SSQL = "";

        DataTable dt2 = new DataTable();
        SSQL = "select MachineID,isnull(DeptName,'') As DeptName,Shift,isnull(FirstName,'') as FirstName,TimeIN,TimeOUT,";
        SSQL = SSQL + "Total_Hrs from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode.ToString() + "'";

        if (fromdate != "")
        {
            SSQL = SSQL + " And  CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(fromdate).ToString("dd/MM/yyyy") + "',103)";
        }
        //if (Division != "-Select-")
        //{
        //    SSQL = SSQL + " And Division = '" + Division + "'";
        //}

        SSQL = SSQL + "  And Shift !='No Shift'  And TimeIN!=''";
        dt2 = objdata.RptEmployeeMultipleDetails(SSQL);



        SSQL = "";

        SSQL = "select MachineID,DeptName,ExistingCode,Shift,isnull(FirstName,'') + '.'+ isnull(LastName,'') as FirstName,TimeIN,TimeOUT";
        SSQL = SSQL + " from LogTime_Days where CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode.ToString() + "'";



        if (fromdate != "")
        {
            SSQL = SSQL + " And  CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(fromdate).ToString("dd/MM/yyyy") + "',103)";
        }



        SSQL = SSQL + " And (TypeName='Proper' or TypeName='Absent') And Shift!='No Shift' And TimeIN!=''";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "select MachineID,DeptName,ExistingCode,Shift,isnull(FirstName,'') as FirstName,TypeName,TimeIN,TimeOUT";
        SSQL = SSQL + " from LogTime_Days   ";
        SSQL = SSQL + " where CompCode='" + SessionCcode.ToString() + "' ANd LocCode='" + SessionLcode.ToString() + "'";

        if (fromdate != "")
        {
            SSQL = SSQL + " And  CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(fromdate).ToString("dd/MM/yyyy") + "',103)";
        }



        SSQL = SSQL + " And TypeName='Improper'";

        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
        SSQL = "Select * from Company_Mst";
        dt3 = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt2.Rows.Count != 0)
        {
            if (dt1.Rows.Count != 0)
            {
                int sno = 1;
                DataTable Emp_dt = new DataTable();
                string category;
                string subCat;
                for (int i = 0; i < dt1.Rows.Count; i++)
                {

                    SSQL = "Select * from ";
                    SSQL = SSQL + " Employee_Mst";
                    //if (status == "Approval")
                    //{
                    //    SSQL = SSQL + " Employee_Mst";
                    //}
                    //else
                    //{
                    //    SSQL = SSQL + " Employee_Mst_New_Emp";
                    //}
                    SSQL = SSQL + " where MachineID='" + dt1.Rows[i]["MachineID"].ToString() + "' ";
                    Emp_dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    category = "";
                    subCat = "";
                    if (Emp_dt.Rows.Count != 0)
                    {
                        category = Emp_dt.Rows[0]["CatName"].ToString();
                        subCat = Emp_dt.Rows[0]["SubCatName"].ToString();
                    }


                    DataCellsImpro.NewRow();
                    DataCellsImpro.Rows.Add();

                    DataCellsImpro.Rows[i]["CompanyName"] = dt3.Rows[0]["CompName"].ToString();
                    DataCellsImpro.Rows[i]["LocationName"] = SessionLcode;
                    DataCellsImpro.Rows[i]["ShiftDate"] = Date;
                    DataCellsImpro.Rows[i]["SNo"] = sno;


                    DataCellsImpro.Rows[i]["Dept"] = dt1.Rows[i]["DeptName"].ToString();
                    DataCellsImpro.Rows[i]["Type"] = dt1.Rows[i]["TypeName"].ToString();
                    DataCellsImpro.Rows[i]["Shift"] = dt1.Rows[i]["Shift"].ToString();
                    DataCellsImpro.Rows[i]["Category"] = category;

                    DataCellsImpro.Rows[i]["SubCategory"] = subCat;
                    DataCellsImpro.Rows[i]["EmpCode"] = dt1.Rows[i]["MachineID"].ToString();
                    DataCellsImpro.Rows[i]["ExCode"] = dt1.Rows[i]["ExistingCode"].ToString();
                    DataCellsImpro.Rows[i]["Name"] = dt1.Rows[i]["FirstName"].ToString();

                    DataCellsImpro.Rows[i]["TimeIN"] = dt1.Rows[i]["TimeIN"].ToString();
                    DataCellsImpro.Rows[i]["TimeOUT"] = dt1.Rows[i]["TimeOUT"].ToString();
                    DataCellsImpro.Rows[i]["MachineID"] = dt1.Rows[i]["MachineID"].ToString();

                    DataCellsImpro.Rows[i]["PrepBy"] = dt2.Rows.Count;
                    DataCellsImpro.Rows[i]["PrepDate"] = dt.Rows.Count;
                    DataCellsImpro.Rows[i]["TotalMIN"] = "";
                    DataCellsImpro.Rows[i]["TotalMIN"] = "";


                    sno = sno + 1;

                }


            }
        }


    }

    public void GetAttdDayWise_Improper_Change()
    {
        DataCellsImpro.Columns.Add("CompanyName");
        DataCellsImpro.Columns.Add("LocationName");
        DataCellsImpro.Columns.Add("ShiftDate");

        DataCellsImpro.Columns.Add("SNo");
        DataCellsImpro.Columns.Add("Dept");
        DataCellsImpro.Columns.Add("Type");
        DataCellsImpro.Columns.Add("Shift");
        DataCellsImpro.Columns.Add("Category");
        DataCellsImpro.Columns.Add("SubCategory");
        DataCellsImpro.Columns.Add("EmpCode");
        DataCellsImpro.Columns.Add("ExCode");
        DataCellsImpro.Columns.Add("Name");
        DataCellsImpro.Columns.Add("TimeIN");
        DataCellsImpro.Columns.Add("TimeOUT");
        DataCellsImpro.Columns.Add("MachineID");
        DataCellsImpro.Columns.Add("PrepBy");
        DataCellsImpro.Columns.Add("PrepDate");
        DataCellsImpro.Columns.Add("TotalMIN");
        DataCellsImpro.Columns.Add("GrandTOT");


        GetImproperPunch();


        ds.Tables.Add(DataCellsImpro);
        ReportDocument report = new ReportDocument();
        report.Load(Server.MapPath("crystal/PresentAbstract.rpt"));

        report.Database.Tables[0].SetDataSource(ds.Tables[0]);

        string SSQL = "";
        DataTable dt2 = new DataTable();

        SSQL = "";
        SSQL = "select LD.MachineID,LD.ExistingCode,isnull(LD.DeptName,'') As DeptName,LD.Shift,LD.TypeName,isnull(LD.FirstName,'') as FirstName,";
        SSQL = SSQL + " LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName from LogTime_Days LD";
        SSQL = SSQL + " inner join ";
        SSQL = SSQL + " Employee_Mst";

        //if (status == "Approval")
        //{
        //    SSQL = SSQL + " Employee_Mst";
        //}
        //else
        //{
        //    SSQL = SSQL + " Employee_Mst_New_Emp";
        //}
        SSQL = SSQL + " EM on EM.MachineID = LD.MachineID";

        SSQL = SSQL + " where LD.CompCode='" + SessionCcode + "' ANd LD.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' ANd EM.LocCode='" + SessionLcode + "'";



        if (fromdate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(fromdate).ToString("dd/MM/yyyy") + "',103)";
        }

        SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";
        dt2 = objdata.RptEmployeeMultipleDetails(SSQL);





        report.DataDefinition.FormulaFields["ShiftDate"].Text = "'" + fromdate + "'";
        report.DataDefinition.FormulaFields["New_Formula"].Text = "'" + dt2.Rows.Count.ToString() + "'";

        //string Customer_Mail_ID = "tpsmhrm@gmail.com";
        //string Agent_Mail_ID = "padmanabhanscoto@gmail.com";


        string[] Check_Val = fromdate.Split(new[] { "/", "_" }, StringSplitOptions.RemoveEmptyEntries);

        string Server_Path = Server.MapPath("~");

        string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
        AttachfileName_Improper = Server_Path + "/Daily_Report/DailyImproper_" + Check_Val[0] + Check_Val[1] + Check_Val[2] + ".pdf";


        if (File.Exists(AttachfileName_Improper))
        {
            File.Delete(AttachfileName_Improper);
        }

        report.ExportToDisk(ExportFormatType.PortableDocFormat, AttachfileName_Improper);


        //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
        //CrystalReportViewer1.ReportSource = report;
    }
    public void GetAttdDayWise_MisMatch_Change()
    {
        DataColumn auto = new DataColumn();
        auto.AutoIncrement = true;
        auto.AutoIncrementSeed = 1;

        DataCellsMiss.Columns.Add("CompanyName");
        DataCellsMiss.Columns.Add("LocationName");
        DataCellsMiss.Columns.Add("ShiftDate");
        DataCellsMiss.Columns.Add("SNo");
        DataCellsMiss.Columns.Add("Dept");
        DataCellsMiss.Columns.Add("Type");
        DataCellsMiss.Columns.Add("Shift");
        DataCellsMiss.Columns.Add("Category");
        DataCellsMiss.Columns.Add("SubCategory");
        DataCellsMiss.Columns.Add("EmpCode");
        DataCellsMiss.Columns.Add("ExCode");
        DataCellsMiss.Columns.Add("Name");
        DataCellsMiss.Columns.Add("TimeIN");
        DataCellsMiss.Columns.Add("TimeOUT");
        DataCellsMiss.Columns.Add("MachineID");
        DataCellsMiss.Columns.Add("PrepBy");
        DataCellsMiss.Columns.Add("PrepDate");
        DataCellsMiss.Columns.Add("TotalMIN");
        DataCellsMiss.Columns.Add("GrandTOT");

        GetShiftMismacth();

        ds.Tables.Add(DataCellsMiss);
        ReportDocument report3 = new ReportDocument();
        report3.Load(Server.MapPath("crystal/AttendanceMisMail.rpt"));
        report3.DataDefinition.FormulaFields["ShiftDate"].Text = "'" + fromdate + "'";
        report3.Database.Tables[0].SetDataSource(ds.Tables[0]);

        //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
        //CrystalReportViewer1.ReportSource = report;


        string[] Check_Val = fromdate.Split(new[] { "/", "_" }, StringSplitOptions.RemoveEmptyEntries);

        string Server_Path = Server.MapPath("~");

        string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
        AttachfileName_Miss = Server_Path + "/Daily_Report/DailyMisMatch_" + Check_Val[0] + Check_Val[1] + Check_Val[2] + ".pdf";

        if (File.Exists(AttachfileName_Miss))
        {
            File.Delete(AttachfileName_Miss);
        }

        report3.ExportToDisk(ExportFormatType.PortableDocFormat, AttachfileName_Miss);
    }

    public void GetShiftMismacth()
    {
        SSQL = "select LD.MachineID,LD.ExistingCode,LD.DeptName,LD.Shift,isnull(LD.FirstName,'') as FirstName,LD.TimeIN,LD.TimeOUT,";
        SSQL = SSQL + "LD.Total_Hrs1 as Total_Hrs,LD.TypeName from LogTime_Days LD";
        SSQL = SSQL + " inner join ";
        SSQL = SSQL + "Employee_Mst";

        //if (status == "Approval")
        //{

        //}
        //else
        //{
        //    SSQL = SSQL + "Employee_Mst_New_Emp";
        //}
        SSQL = SSQL + " EM on EM.MachineID = LD.MachineID";
        SSQL = SSQL + " where LD.CompCode='" + SessionCcode + "' ANd LD.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' ANd EM.LocCode='" + SessionLcode + "'";

        if (fromdate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(fromdate).ToString("dd/MM/yyyy") + "',103)";
        }

        SSQL = SSQL + " And LD.Shift='No Shift' And LD.TimeIN <> ''";
        SSQL = SSQL + " And (LD.TypeName='GENERAL Mismatch' or LD.TypeName='Mismatch' or LD.TypeName='Improper')";

        Logindays = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "Select * from Company_Mst";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);


        DataTable Emp_dt = new DataTable();
        string category;
        string subCat;
        if (Logindays.Rows.Count != 0)
        {
            int sno = 1;
            for (int i = 0; i < Logindays.Rows.Count; i++)
            {

                SSQL = "Select * from ";
                SSQL = SSQL + "Employee_Mst";

                SSQL = SSQL + " where MachineID='" + Logindays.Rows[i]["MachineID"].ToString() + "' ";
                Emp_dt = objdata.RptEmployeeMultipleDetails(SSQL);

                category = "";
                subCat = "";
                if (Emp_dt.Rows.Count != 0)
                {
                    category = Emp_dt.Rows[0]["CatName"].ToString();
                    subCat = Emp_dt.Rows[0]["SubCatName"].ToString();
                }

                DataCellsMiss.NewRow();
                DataCellsMiss.Rows.Add();




                DataCellsMiss.Rows[i]["CompanyName"] = dt.Rows[0]["CompName"].ToString();
                DataCellsMiss.Rows[i]["LocationName"] = SessionLcode;
                DataCellsMiss.Rows[i]["ShiftDate"] = Date;
                DataCellsMiss.Rows[i]["SNo"] = sno;

                DataCellsMiss.Rows[i]["Dept"] = Logindays.Rows[i]["DeptName"].ToString();
                DataCellsMiss.Rows[i]["Type"] = Logindays.Rows[i]["TypeName"].ToString();
                DataCellsMiss.Rows[i]["Shift"] = Logindays.Rows[i]["Shift"].ToString();
                DataCellsMiss.Rows[i]["Category"] = category.ToString();
                DataCellsMiss.Rows[i]["SubCategory"] = subCat.ToString();
                DataCellsMiss.Rows[i]["EmpCode"] = Logindays.Rows[i]["MachineID"].ToString();


                DataCellsMiss.Rows[i]["ExCode"] = Logindays.Rows[i]["ExistingCode"].ToString();
                DataCellsMiss.Rows[i]["Name"] = Logindays.Rows[i]["FirstName"].ToString();
                DataCellsMiss.Rows[i]["TimeIN"] = Logindays.Rows[i]["TimeIN"].ToString();
                DataCellsMiss.Rows[i]["TimeOUT"] = Logindays.Rows[i]["TimeOUT"].ToString();
                DataCellsMiss.Rows[i]["MachineID"] = Logindays.Rows[i]["MachineID"].ToString();
                DataCellsMiss.Rows[i]["PrepBy"] = "User";
                DataCellsMiss.Rows[i]["PrepDate"] = Date;
                DataCellsMiss.Rows[i]["TotalMIN"] = "";
                DataCellsMiss.Rows[i]["GrandTOT"] = Logindays.Rows[i]["Total_Hrs"].ToString();


                sno = sno + 1;
            }
        }
    }



    protected void Page_Unload(object sender, EventArgs e)
    {
        //CrystalReportViewer1.Dispose();
    }


}
